/*
 * =====================================================================================
 *
 *       Filename:  usb.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-2-12 8:29:03
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
void Endpt0_OutInt();
void Endpt0_InInt();
void Endpt1_OutInt();
void Endpt2_OutInt();
void Endpt1_InInt();
void Endpt2_InInt();
void USB_Rst();
void USB_Task();
