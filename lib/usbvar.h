/*
 * =====================================================================================
 *
 *       Filename:  USBDES.H
 *
 *    Description:  :bp
 *
 *
 *        Version:  1.0
 *        Created:  2010-2-11 18:14:46
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */


#ifndef  USBVAR_INC
#define  USBVAR_INC
#include "usbdes.h"
extern code uint8 devDes[DEVDES_LEN];
extern uint8 interruptState;
extern StruCmdBuf  idata ep0RxBuf;
#endif   /* ----- #ifndef USBDES_INC  ----- */
