/*
 * =====================================================================================
 *
 *       Filename:  Fifo.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2009-12-29 23:44:09
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#include "..\include\bitopt.h"
#include "..\include\Fifo.h"

#define		NOEMPTY 0x01   /* 非空标志 */
#define		FULL    0x02  /* 满标志` */



/* 
* ===  FUNCTION  ======================================================================
 *         Name:  InitFifo
 *  Description:  
 * =====================================================================================
 */
void Fifo_Init(Fifo *fifo,uint8 *buf,uint8 len)
{
	fifo->buf=buf;
	fifo->len=len;
	fifo->rdPoint=0;
	fifo->wrPoint=0;
	fifo->flag = 0;                               /* �?*/
	
}		/* -----  end of function InitFifo  ----- */




/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  CheckNoEmpty
 *  Description:  
 * =====================================================================================
 */

unsigned char Fifo_CheckNoEmpty ( Fifo *fifo )
{
	return CHKBIT(fifo->flag,NOEMPTY);
}		/* -----  end of function CheckNoEmpty  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  CheckFull
 *  Description:  
 * =====================================================================================
 */
 unsigned char Fifo_CheckFull (Fifo *fifo ) 
{
	return CHKBIT(fifo->flag,FULL);
}		/* -----  end of function CheckFull  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  ReadFifo
 *  Description:  读取成功返回1,失败返回0
 * =====================================================================================
 */
uint8  Fifo_ReadFifo (Fifo *fifo,uint8 *rdChar) reentrant 

{
	if(Fifo_CheckNoEmpty(fifo))
	{
		*rdChar = fifo->buf[(fifo->rdPoint)&(fifo->len-1)];
		fifo->rdPoint++;
		CLRBIT(fifo->flag,FULL);         /* 读取1个字节后 fifo 至少
						   有一个字节的空间*/
		if(fifo->rdPoint==fifo->wrPoint)
		{
			CLRBIT(fifo->flag,NOEMPTY);
		}
		return 1;

	}
	return 0;
}		/* -----  end of function READFIFO  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Fifo_WriteFifo
 *  Description:  
 * =====================================================================================
 */
 void Fifo_WriteFifo (Fifo *fifo,unsigned char wrChar) reentrant
{
        SETBIT(fifo->flag,NOEMPTY);
	if(Fifo_CheckFull(fifo))
	{
               
		return ;
	}
	else
	{
		fifo->buf[fifo->wrPoint&(fifo->len-1)]=wrChar;
		fifo->wrPoint++;
                 if(fifo->wrPoint-fifo->rdPoint>=fifo->len)
		{
			SETBIT(fifo->flag,FULL);
		}
	        return ;
	}
}		/* -----  end of function Fifo_WriteFifo  ----- */
