/*
 * =====================================================================================
 *
 *       Filename:  key.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-2-20 8:36:55
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

void Key_Task();
uint8 Key_ChangeChk();
uint8 Key_GetValue();
