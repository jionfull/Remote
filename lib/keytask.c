/*
 * =====================================================================================
 *
 *       Filename:  keytask.c
 *
 *    Description:  key
 *
 *        Version:  1.0
 *        Created:  2010-2-20 8:10:17
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#include "..\include\typedef.h"
#include "..\include\bitopt.h"
#include	"..\sys\GlobalVariable.h"
#include	"..\include\Gpio.h"
#include "..\include\serial.h"
#include "keyCfg.h"

static uint8 keyChangeBit;
static uint8 keyValue;
void key_task()
{
	static  uint8 currticks=0;
	uint8 tempKey;
	if(((uint8)ticks)-currticks>TASKPERIOD)
	{
		currticks=(uint8)ticks;
		Gpio_In(KEYPORT,&tempKey);
//	Gpio_Out(2,tempKey);
	    if(keyValue!=tempKey)
		{
//			Serial_Send(keyValue);
//			Serial_Send(tempKey);
			keyChangeBit=(keyValue^tempKey);
	
//			Serial_Send(keyValue^tempKey);
			keyValue=tempKey;
		}
	
	
	}
}
uint8 Key_ChangeChk()
{
       return keyChangeBit;
}
uint8 Key_GetValue()
{
	keyChangeBit=0;
	//Gpio_Out(2,keyValue);
	return ~keyValue;
}
