/*
 * =====================================================================================
 *
 *       Filename:  keyCfg.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-2-20 8:33:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  KEYCFG_INC
#define  KEYCFG_INC

#define	KEYCHANGE 0x01			/*  */

#define	KEYPORT 0x01			/*  */

#define	TASKPERIOD 10			/*  */
#endif   /* ----- #ifndef KEYCFG_INC  ----- */
