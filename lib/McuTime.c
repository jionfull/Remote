/*
 * =====================================================================================
 *
 *       Filename:  McuTime.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-7 14:51:11
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */
#include "..\include\McuTime.h"
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  McuTime_AddHour
 *  Description:  
 * =====================================================================================
 */
void McuTime_AddHour(McuTime * time,unsigned char hour)       /* 暂时须小于24 */
{
		hour += time->hour;
		if(hour>=24)
		{
				time->hour  =  hour-24;
		}
		else
		{
				time->hour  =  hour;
		}
}
void McuTime_AddMin(McuTime * time,unsigned char min)         /* 暂时须小于60 */
{
		min += time->min;
		if(min>=60)
		{
				time->min  =  min-60;
				McuTime_AddHour(time,1);
		}
		else
		{
				time->min  =  min;
		}
}
void McuTime_AddSec(McuTime * time,unsigned char sec)         /* 暂时须小于60 */
{
		sec += time->sec;
		if(sec>=60)
		{
				time->sec  =  sec-60;
				McuTime_AddMin(time,1);
		}
		else
		{
				time->sec  =  sec;
		}
}
