/*
 * =====================================================================================
 *
 *       Filename:  SEG7.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-7 14:23:57
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  SEG7_H_INC
#define  SEG7_H_INC

void Seg7_DisplayLoop ();
void Seg7_WriteChar(unsigned char addr,unsigned char wrChar);
void Seg7_SetNextCharFlag ();
#endif   /* ----- #ifndef SEG7_H_INC  ----- */
