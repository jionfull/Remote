/*
 * =====================================================================================
 *
 *       Filename:  SEG7.c
 *
 *    Description:  7 段数码管
 *
 *        Version:  1.0
 *        Created:  2010-1-7 11:48:12
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#include "SEG7CONFIG.h"
#include	"..\include\bitopt.h"
#include	"..\include\Gpio.h"

#ifndef ZEROBIT_AT_MIDILE
const unsigned char zfList[16]={ 0x3f,0x06,0x5b,0x4f,
								0x66,0x6d,0x7d,0x07,
								0x7f,0x6f,0x77,0x7c,
								0x39,0x5e,0x79,0x71};

#else
const unsigned char zfList[16]={ 0x7E,0x30,0x6d,0x79,
                        	0x33,0x5b,0x5f,0x70,
				0x7f,0x73,0x77,0x1f,
								0x0d,0x3d,0x4f,0x17};

#endif

const unsigned char smList[SEG7MAX]={0xfe,0xfd,0xfb,0xf7,0xef,0xdf,0xbf,0x7f};


/*-----------------------------------------------------------------------------
 *  私有变量
 *-----------------------------------------------------------------------------*/
unsigned char flag;
unsigned char seg7DisplayCount;
unsigned char buf[SEG7MAX];

/*-----------------------------------------------------------------------------
 *  显示循环
 *-----------------------------------------------------------------------------*/
void Seg7_DisplayLoop (  )
{
		if(CHKBIT(flag,NEXTCHAR))
		{
				CLRBIT(flag,NEXTCHAR);
				if(seg7DisplayCount>=SEG7MAX)
				{
						seg7DisplayCount=0;
				}
				Gpio_SetBit(PORTSM,CTLSM);
				Gpio_Out(PORTCHAROUT,buf[seg7DisplayCount]);
				Gpio_Out(PORTSM,~smList[seg7DisplayCount]);
				seg7DisplayCount++;
		}
}		/* -----  end of function   ----- */
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Seg7_WriteChar
 *  Description:  向Seg7 写一个字�? * =====================================================================================
 */
void Seg7_WriteChar (unsigned char addr,unsigned char wrChar)
{
		if(addr<SEG7MAX&&wrChar<16)
		{
#ifdef COMP
		 buf[addr]=-zfList[wrChar];
#else
		 buf[addr]=zfList[wrChar];
#endif
		}
}		/* -----  end of function Seg7_WriteChar  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  Seg7_WriteChar
 *  Description:  设置标志,此函数应在定时中断中调用
 * =====================================================================================
 */
void Seg7_SetNextCharFlag (  )
{
		SETBIT(flag,NEXTCHAR);
}		/* -----  end of function Seg7_WriteChar  ----- */
