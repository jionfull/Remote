/*
 * =====================================================================================
 *
 *       Filename:  usbdes.c
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  2010-2-10 18:16:23
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *        Company:
 *
 * =====================================================================================
 */
#include	"USBDESCFG.H"
#include "..\include\typedef.h"
 uint8 code DevDes[DEVDES_LEN] =
{
	DEVDES_LEN,
	DEVDES_TYPENUM,
	USBVERL,
	USBVERH,
	DEVDES_DEVCLASS,
	DEVDES_DEVSUBCLASS,
	DEVDES_DEVPROTO,
	DEVDES_MAXPACKETSZ0,
	VENDER_IDL,
	VENDER_IDH,
	DEV_IDL,
	DEV_IDH,
	DEV_VERL,
	DEV_VERH,
	VENDER_STRIDX,
	DEV_STRIDX,
	SN_STRIDX,
	CFGMAX
};
//USB报告描述符的定义
code uint8 ReportDes[52]=
{
 0x05, 0x01, // USAGE_PAGE (Generic Desktop)
 0x09, 0x02, // USAGE (Mouse)
 0xa1, 0x01, // COLLECTION (Application)
	 0x09, 0x01, //   USAGE (Pointer)
	 0xa1, 0x00, //   COLLECTION (Physical)
		 0x05, 0x09, //     USAGE_PAGE (Button)
		 0x19, 0x01, //     USAGE_MINIMUM (Button 1)
		 0x29, 0x03, //     USAGE_MAXIMUM (Button 3)
		 0x15, 0x00, //     LOGICAL_MINIMUM (0)
		 0x25, 0x01, //     LOGICAL_MAXIMUM (1)
		 0x95, 0x03, //     REPORT_COUNT (3)
		 0x75, 0x01, //     REPORT_SIZE (1)
		 0x81, 0x02, //     INPUT (Data,Var,Abs)
		 0x95, 0x01, //     REPORT_COUNT (1)
		 0x75, 0x05, //     REPORT_SIZE (5)
		 0x81, 0x03, //     INPUT (Cnst,Var,Abs)
		 0x05, 0x01, //     USAGE_PAGE (Generic Desktop)
		 0x09, 0x30, //     USAGE (X)
		 0x09, 0x31, //     USAGE (Y)
		 0x09, 0x38, //     USAGE (Wheel)
		 0x15, 0x81, //     LOGICAL_MINIMUM (-127)
		 0x25, 0x7f, //     LOGICAL_MAXIMUM (127)
		 0x75, 0x08, //     REPORT_SIZE (8)
		 0x95, 0x03, //     REPORT_COUNT (3)
		 0x81, 0x06, //     INPUT (Data,Var,Rel)
	 0xc0,       //   END_COLLECTION
 0xc0        // END_COLLECTION
};
//通过上面的报告描述符的定义，我们知道返回的输入报告具有4字节。
//第一字节的低3位用来表示按键是否按下的，高5位为常数0，无用。
//第二字节表示X轴改的变量，第三字节表示Y轴的改变量，第四字节表示
//滚轮的改变量。我们在中断端点1中应该要按照上面的格式返回实际的
//鼠标数据。
///////////////////////////报告描述符完毕////////////////////////////

/*-----------------------------------------------------------------------------
 *  Configuration Descriptor 
 *-----------------------------------------------------------------------------*/
code uint8 CfgDes[CFGDES_LEN+ITFDES_LEN+HIDDES_LEN+EPDES_LEN]=
{
 
	/*-----------------------------------------------------------------------------
	 *  Configuration Descriptor 
	 *-----------------------------------------------------------------------------*/
	CFGDES_LEN,
	CFGDES_TYPENUM,
	 (CFGDES_LEN+ITFDES_LEN+HIDDES_LEN+EPDES_LEN)&0xFF, 
	 ((CFGDES_LEN+ITFDES_LEN+HIDDES_LEN+EPDES_LEN)>>8)&0xFF,
	CFGDES_ITFMAX,
	CFGDES_CFGVALUE,
	CFGDES_STRIDX,
	CFGDES_PWRES,                           /*如为自行供电 +CFGDES_PWSELF  */
	CFGDES_CURRMAX,

 
	/*-----------------------------------------------------------------------------
	 *  Interface Descriptor 
	 *-----------------------------------------------------------------------------*/
	ITFDES_LEN,
	ITFDES_TYPENUM,
	ITFDES_NUM,
	ITFDES_ALTERNUM,
	ITFDES_EPMAX,
 	ITFDES_CLASS_HID,
	ITFDES_SUBCLASS_BOOT,
	ITFDES_PROTOCOL_MOUSE,
	ITFDES_STRIDX,
 
 
	/*-----------------------------------------------------------------------------
	 *  HID Descriptor
	 *-----------------------------------------------------------------------------*/
	HIDDES_LEN,
	HIDDES_TYPENUM,
	HIDDES_VERL,
	HIDDES_VERH,
	HIDDES_COUNTY_USA,
	HIDDES_SUBDESMAX,
	HIDDES_SUBDESTYPENO,
 	sizeof(ReportDes)&0xFF,
       (sizeof(ReportDes)>>8)&0xFF,
 
       /*-----------------------------------------------------------------------------
	*  Endpoint Descriptor
	*-----------------------------------------------------------------------------*/
       EPDES_LEN,
       EPDES_TYPENUM,
       EPDES_EPDIRIN+EPDES_EPADDR,
       EPDES_ATTR_INTTR,
       EPDES_MAXPACKETSZ1L,
       EPDES_MAXPACKETSZ1H,
       EPDES_INTERVAL,
};
////////////////////////配置描述符集合完毕//////////////////////////

/************************语言ID的定义********************/
code uint8 StrDes_LanguageId[4]=
{
 0x04, //本描述符的长度
 0x03, //字符串描述符
 //0x0409为美式英语的ID
 0x09,
 0x04
};
////////////////////////语言ID完毕//////////////////////////////////

code uint8 StrDes_Vender[0x08]={  
0x08,       /*Len*/
0x03,       /*StringDescriptor Num*/
         /*    */
0x20, 0x5f,          /*    */
0x6f, 0x66,          /*    */
0x8f, 0x79};/*end of */

code uint8 StrDes_Product[0x0c]={  
0x0c,       /*Len*/
0x03,       /*StringDescriptor Num*/
         /*    */
0x44, 0x00,          /*    */
0x49, 0x00,          /*    */
0x59, 0x00,          /*    */
0x20, 0x9f,          /*    */
0x07, 0x68};/*end of */ 

code uint8 StrDes_SN[0x1c]={  
0x1c,       /*Len*/
0x03,       /*StringDescriptor Num*/
         /*    */
0x32, 0x00,          /*    */
0x30, 0x00,          /*    */
0x31, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x37, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x32, 0x00 };/*end of */
