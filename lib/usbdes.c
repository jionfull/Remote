/*
 * =====================================================================================
 *
 *       Filename:  usbdes.c
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  2010-2-10 18:16:23
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (),
 *        Company:
 *
 * =====================================================================================
 */
/*-----------------------------------------------------------------------------
Usage Page (Generic Desktop), 
Usage (Keyboard),  
Collection (Application), 
 Usage Page (Key Codes); 
 Usage Minimum (224),  
 Usage Maximum (231), 
 Logical Minimum (0),  
 Logical Maximum (1),  
 Report Size (1),  
 Report Count (8),  
 Input (Data, Variable, Absolute),  ;Modifier byte 
 Report Count (1),  
 Report Size (8), 
 Input (Constant),    ;Reserved byte 
 Report Count (5),  
 Report Size (1),  
 Usage Page (Page# for LEDs),  
 Usage Minimum (1),  
 Usage Maximum (5), 
 Output (Data, Variable, Absolute), ;LED report 
 Report Count (1),  
 Report Size (3), 
 Output (Constant),   ;LED report padding 
 Report Count (6),  
 Report Size (8),  
 Logical Minimum (0),  
 Logical Maximum(101), 
 Usage Page (Key Codes),  
 Usage Minimum (0), 
 Usage Maximum (101),  
 Input (Data, Array),    ;Key arrays (6 bytes) 
End Collection 
05 01 
09 06 
A1 01 
05 07 
19 E0 
29 E7 
15 00 
25 01 
75 01 
95 08 
81 02 
95 01 
75 08 
81 01 
95 05 
75 01 
05 08 
19 01 
29 05 
91 02 
95 01 
75 03 
91 01 
95 06 
75 08 
15 00 
25 65 
05 07 
19 00 
29 65 
81 00 
C0 
 
 *  
 *-----------------------------------------------------------------------------*/
#include	"USBDESCFG.H"
#include "..\include\typedef.h"
 uint8 code DevDes[DEVDES_LEN] =
{
	DEVDES_LEN,
	DEVDES_TYPENUM,
	USBVERL,
	USBVERH,
	DEVDES_DEVCLASS,
	DEVDES_DEVSUBCLASS,
	DEVDES_DEVPROTO,
	DEVDES_MAXPACKETSZ0,
	VENDER_IDL,
	VENDER_IDH,
	DEV_IDL,
	DEV_IDH,
	DEV_VERL,
	DEV_VERH,
	VENDER_STRIDX,
	DEV_STRIDX,
	SN_STRIDX,
	CFGMAX
};
//USB报告描述符的定义E
//USB报告描述符的定义
code uint8 ReportDes[67]=
{

 0x05, 0x01, // USAGE_PAGE (Generic Desktop)
 0x09, 0x06, // USAGE (Keyboard)
 0xa1, 0x01, // COLLECTION (Application)
 0x85, 0x01, //Report id =1
 0x05, 0x07, //     USAGE_PAGE (Keyboard/Keypad)
 0x19, 0xe0, //     USAGE_MINIMUM (Keyboard LeftControl)
 0x29, 0xe7, //     USAGE_MAXIMUM (Keyboard Right GUI)
 0x15, 0x00, //     LOGICAL_MINIMUM (0)
 0x25, 0x01, //     LOGICAL_MAXIMUM (1)
 0x95, 0x08, //     REPORT_COUNT (8)
 0x75, 0x01, //     REPORT_SIZE (1)
 0x81, 0x02, //     INPUT (Data,Var,Abs)
 0x95, 0x01, //     REPORT_COUNT (1)
 0x75, 0x08, //     REPORT_SIZE (8)
 0x81, 0x01, //     INPUT (Cnst,Var,Abs)
 0x95, 0x06, //   REPORT_COUNT (6)
 0x75, 0x08, //   REPORT_SIZE (8)
 0x15, 0x00, //   LOGICAL_MINIMUM (0)
 0x25, 0xFF, //   LOGICAL_MAXIMUM (255)
 0x05, 0x07, //   USAGE_PAGE (Keyboard/Keypad)
 0x19, 0x00, //   USAGE_MINIMUM (Reserved (no event indicated))
 0x29, 0x65, //   USAGE_MAXIMUM (Keyboard Application)
 0x81, 0x00, //     INPUT (Data,Ary,Abs)
 0x25, 0x01, //     LOGICAL_MAXIMUM (1)
 0x95, 0x05, //   REPORT_COUNT (5)
 0x75, 0x01, //   REPORT_SIZE (1)
 0x05, 0x08, //   USAGE_PAGE (LEDs)
 0x19, 0x01, //   USAGE_MINIMUM (Num Lock)
 0x29, 0x05, //   USAGE_MAXIMUM (Kana)
 0x91, 0x02, //   OUTPUT (Data,Var,Abs)
 0x95, 0x01, //   REPORT_COUNT (1)
 0x75, 0x03, //   REPORT_SIZE (3)
 0x91, 0x03, //   OUTPUT (Cnst,Var,Abs)
 0xc0        // END_COLLECTION
};

	 
	 /*-----------------------------------------------------------------------------
	  *  以下为鼠标部分 
	  *-----------------------------------------------------------------------------*/
code uint8 reportDes_Mouse[54]=
{
 0x05, 0x01, // USAGE_PAGE (Generic Desktop)
 0x09, 0x02, // USAGE (Mouse)
 0xa1, 0x01, // COLLECTION (Application)
 	 0x85, 0x01,                            /*   Report Id(02) */
	 0x09, 0x01, //   USAGE (Pointer)
	 0xa1, 0x00, //   COLLECTION (Physical)
		 0x05, 0x09, //     USAGE_PAGE (Button)
		 0x19, 0x01, //     USAGE_MINIMUM (Button 1)
		 0x29, 0x03, //     USAGE_MAXIMUM (Button 3)
		 0x15, 0x00, //     LOGICAL_MINIMUM (0)
		 0x25, 0x01, //     LOGICAL_MAXIMUM (1)
		 0x95, 0x03, //     REPORT_COUNT (3)
		 0x75, 0x01, //     REPORT_SIZE (1)
		 0x81, 0x02, //     INPUT (Data,Var,Abs)
		 0x95, 0x01, //     REPORT_COUNT (1)
		 0x75, 0x05, //     REPORT_SIZE (5)
		 0x81, 0x03, //     INPUT (Cnst,Var,Abs)
		 0x05, 0x01, //     USAGE_PAGE (Generic Desktop)
		 0x09, 0x30, //     USAGE (X)
		 0x09, 0x31, //     USAGE (Y)
		 0x09, 0x38, //     USAGE (Wheel)
		 0x15, 0x81, //     LOGICAL_MINIMUM (-127)
		 0x25, 0x7f, //     LOGICAL_MAXIMUM (127)
		 0x75, 0x08, //     REPORT_SIZE (8)
		 0x95, 0x03, //     REPORT_COUNT (3)
		 0x81, 0x06, //     INPUT (Data,Var,Rel)
	 0xc0,       //   END_COLLECTION
 0xc0        // END_COLLECTION
};


/*-----------------------------------------------------------------------------
 *  Configuration Descriptor 
 *-----------------------------------------------------------------------------*/
code uint8 CfgDes[CFGDES_LEN+ITFDES_LEN+HIDDES_LEN+EPDES_LEN
		+ITFDES_LEN+HIDDES_LEN+EPDES_LEN+EPDES_OUT_LEN]=
{
 
	/*-----------------------------------------------------------------------------
	 *  Configuration Descriptor 
	 *-----------------------------------------------------------------------------*/
	CFGDES_LEN,
	CFGDES_TYPENUM,
	// (CFGDES_LEN+ITFDES_LEN+HIDDES_LEN+EPDES_LEN+EPDES_OUT_LEN)&0xFF, 
	// ((CFGDES_LEN+ITFDES_LEN+HIDDES_LEN+EPDES_LEN+EPDES_OUT_LEN)>>8)&0xFF,
	sizeof(CfgDes)&0xff,
	(sizeof(CfgDes)>>8)&0xff,
	CFGDES_ITFMAX,
	CFGDES_CFGVALUE,
	CFGDES_STRIDX,
	CFGDES_PWRES,                           /*如为自行供电 +CFGDES_PWSELF  */
	CFGDES_CURRMAX,

 
	/*-----------------------------------------------------------------------------
	 *  Interface Descriptor 
	 *-----------------------------------------------------------------------------*/
	ITFDES_LEN,
	ITFDES_TYPENUM,
	ITFDES_NUM,
	ITFDES_ALTERNUM,
	ITFDES_EPMAX,
 	ITFDES_CLASS_HID,
	ITFDES_SUBCLASS_BOOT,
	ITFDES_PROTOCOL_KEYBRD,
	ITFDES_STRIDX,
 
 
	/*-----------------------------------------------------------------------------
	 *  HID Descriptor
	 *-----------------------------------------------------------------------------*/
	HIDDES_LEN,
	HIDDES_TYPENUM,
	HIDDES_VERL,
	HIDDES_VERH,
	HIDDES_COUNTY_USA,
	HIDDES_SUBDESMAX,
	HIDDES_SUBDESTYPENO,
 	sizeof(ReportDes)&0xFF,
       (sizeof(ReportDes)>>8)&0xFF,
 
       /*-----------------------------------------------------------------------------
	*  Endpoint Descriptor
	*-----------------------------------------------------------------------------*/
       EPDES_LEN,
       EPDES_TYPENUM,
       EPDES_EPDIRIN+EPDES_EPADDR,
       EPDES_ATTR_INTTR,
       EPDES_MAXPACKETSZ1L,
       EPDES_MAXPACKETSZ1H,
       EPDES_INTERVAL,

       EPDES_LEN,
       EPDES_TYPENUM,
       EPDES_EPADDR,
       EPDES_ATTR_INTTR,
       EPDES_MAXPACKETSZ1L,
       EPDES_MAXPACKETSZ1H,
       EPDES_INTERVAL,
	/*-----------------------------------------------------------------------------
	 *  Interface Descriptor 
	 *-----------------------------------------------------------------------------*/
	ITFDES_LEN,
	ITFDES_TYPENUM,
	ITFDES_NUM+1,
	ITFDES_ALTERNUM,
	0x01,            //ITFDES_EPMAX,
 	ITFDES_CLASS_HID,
	ITFDES_SUBCLASS_BOOT,
	ITFDES_PROTOCOL_MOUSE,
	ITFDES_STRIDX,
 
 
	/*-----------------------------------------------------------------------------
	 *  HID Descriptor
	 *-----------------------------------------------------------------------------*/
	HIDDES_LEN,
	HIDDES_TYPENUM,
	HIDDES_VERL,
	HIDDES_VERH,
	HIDDES_COUNTY_USA,
	HIDDES_SUBDESMAX,
	HIDDES_SUBDESTYPENO,
 	sizeof(reportDes_Mouse)&0xFF,
       (sizeof(reportDes_Mouse)>>8)&0xFF,
 
       /*-----------------------------------------------------------------------------
	*  Endpoint Descriptor
	*-----------------------------------------------------------------------------*/
       EPDES_LEN,
       EPDES_TYPENUM,
       EPDES_EPDIRIN+EPDES_EPADDR+1,
       EPDES_ATTR_INTTR,
       EPDES_MAXPACKETSZ1L,
       EPDES_MAXPACKETSZ1H,
       EPDES_INTERVAL

      
};
////////////////////////配置描述符集合完毕//////////////////////////

/************************语言ID的定义********************/
code uint8 StrDes_LanguageId[4]=
{
 0x04, //本描述符的长度
 0x03, //字符串描述符
 //0x0409为美式英语的ID
 0x09,
 0x04
};
////////////////////////语言ID完毕//////////////////////////////////

code uint8 StrDes_Vender[0x08]={  
0x08,       /*Len*/
0x03,       /*StringDescriptor Num*/
         /*    */
0x20, 0x5f,          /*    */
0x6f, 0x66,          /*    */
0x8f, 0x79};/*end of */

code uint8 StrDes_Product[0x0c]={  
0x0c,       /*Len*/
0x03,       /*StringDescriptor Num*/
         /*    */
0x44, 0x00,          /*    */
0x49, 0x00,          /*    */
0x59, 0x00,          /*    */
0x20, 0x9f,          /*    */
0x07, 0x68};/*end of */ 

code uint8 StrDes_SN[0x1c]={  
0x1c,       /*Len*/
0x03,       /*StringDescriptor Num*/
         /*    */
0x32, 0x00,          /*    */
0x30, 0x00,          /*    */
0x31, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x37, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x30, 0x00,          /*    */
0x33, 0x00 };/*end of */
