/*
 * =====================================================================================
 *
 *       Filename:  usbdef.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-2-15 15:44:37
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  USBDEF_INC
#define  USBDEF_INC
#include "..\include\typedef.h"

#define	STATUS_CFGOK    (0x01)			/*  */
#define	STATUS_EP1RDY    (0x02)			/*  */
#define	STATUS_EP2RDY    (0x04)			/*  */
#define REQSTL_MASKBIT (0x60)
#define REQ_STD        (0x00)
#define REQ_CLASS      (0x20)
#define REQ_VENDER     (0x40)
#define REQ_RES        (0x60)

#define	STDREQ_GETCFG  (0x08)			/*  */
#define	STDREQ_GETSTATUS  (0x00)			/*  */
#define	STDREQ_GETITF   (0x0a)			/*  */
#define STDREQ_GETDES   (0x06)
#define	STDREQ_SYNCFRM	(0x0c)		/*  */
#define	STDREQ_CLRFEATURE (0x01)			/*  */
#define	STDREQ_SETFEATURE (0x03)			/*  */
#define	STDREQ_SETADDR  (0x05)			/*  */
#define	STDREQ_SETDES	(0x07)		/*  */
#define	STDREQ_SETCFG (0x09)			/*  */
#define	STDREQ_SETITF (0x0b)			/*  */

#define    CLASSREQ_GETREPORT  0x01
#define    CLASSREQ_GETIDLE    0x02 
#define    CLASSREQ_GETPROTOCOL 0x03
#define    CLASSREQ_SETREPORT  0x09
#define    CLASSREQ_SETIDLE 0x0A
#define    CLASSREQ_SETPROTOCOL 0x0B


#define DES_DEV     (0x01)
#define DES_CFG     (0x02)
#define DES_STR     (0x03)
#define DES_ITF     (0x04)
#define DES_ENDPT    (0x05)
#define	DES_REPORT   (0x22)			/*  */

#define REQBUF_TYPEIDX 0x00
#define REQBUF_REQIDX     0x01
#define	REQBUF_VALUELIDX     0x02
#define REQBUF_VALUEHIDX     0x03
#define	REQBUF_IDXLIDX       0x04
#define	REQBUF_IDXHIDX       0x05
#define	REQBUF_LENLIDX       0x06
#define	REQBUF_LENHIDX       0x07
typedef struct 
{
	uint8 *waitPoint;
	uint8 waitLen;	
	uint8 status;
}StruWaitForSend;
#define	SENDSTATUS_NEEDZERO 0x01	/*  */
#define	SENDSTATUS_BUSY     0x02	/*  */

#endif   /* ----- #ifndef USBDEF_INC  ----- */
