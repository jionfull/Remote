/*
 * =====================================================================================
 *
 *       Filename:  time.h
 *
 *    Description:  时间
 *
 *        Version:  1.0
 *        Created:  2010-1-7 14:36:55
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

typedef struct McuTime_Stru
{
		unsigned char sec;
		unsigned char min;
		unsigned char hour;
}McuTime;

void McuTime_AddSec(McuTime *time,unsigned char sec);         /* 暂时须小于60 */
void McuTime_AddMin(McuTime *time,unsigned char min);         /* 暂时须小于60 */
void McuTime_AddHour(McuTime *time,unsigned char hour);       /* 暂时须小于24 */
