/*
 * =====================================================================================
 *
 *       Filename:  busopt.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-31 15:28:57
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  _BUS_OPT_H_INC
#define  _BUS_OPT_H_INC
#include "typedef.h"
bus_data_t bus_rd(bus_addr_t addr);
void bus_wr(bus_addr_t addr,bus_data_t value);
#endif   /* ----- #ifndef _BUS_OPT_H_INC  ----- */
