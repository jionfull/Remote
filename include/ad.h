/*
 * =====================================================================================
 *
 *       Filename:  ad.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-8-1 15:54:27
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  AD_INC
#define  AD_INC
#include "typedef.h"

void Ad_Init();//
void Ad_Start();//
int16 Ad_GetValue();
void Ad_Start();//
void Ad_Task();//


#endif   /* ----- #ifndef AD_INC  ----- */
