/*
 *define some bit option
 *zh_cn:定义一些位操作宏虑函数;
 *author:zjf;  E_mail:jionfull@163.com.  zh_cn:张景�?E_mail:jionfull@163.com
 *editor:CodeBlock
 *$Id: bitopt.h,v 1.0 2009-03-02 06:54:10+08 jionfull Exp jionfull $
 *$Log: bitopt.h,v $
 *Revision 1.0  2009-03-02 06:54:10+08  jionfull
 *Initial revision
 *
 *Note:if you change the code please mail to me & tell me why.
 */



#ifndef BITOPT_H
#define BITOPT_H

#ifndef BIT0
#define  BIT0 0x01
#define  BIT1 0x02
#define  BIT2 0x04
#define  BIT3 0x08
#define  BIT4 0x10
#define  BIT5 0x20
#define  BIT6 0x40
#define  BIT7 0x80
#endif

#define SETBIT(ByteVar,BitVar) (ByteVar |= BitVar)
#define CLRBIT(ByteVar,BitVar) (ByteVar &= ~BitVar)
#define XORBIT(ByteVar,BitVar) (ByteVar ^= BitVar)
#define CHKBIT(ByteVar,BitVar) (ByteVar &  BitVar)

#define LOW(x) (x & 0x00ff)
#define HIGH(x) (x>>8)

#endif 
