/*
 * =====================================================================================
 *
 *       Filename:  I2C.h
 *
 *    Description:  24C512 ��д����
 *
 *        Version:  1.0
 *        Created:  2010-7-16 7:52:48
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */
#ifndef I2C_INC
#define I2C_INC

#include "typedef.h"

void I2C_Init();
void I2C_WriteWord(uint16 addr, uint16 val);
void I2C_ReadBytes(uint16 addr, uint8 *dst,uint8 len);
void I2C_WriteBytes(uint16 addr, uint8 *src,uint8 len);
uint16  I2C_ReadWord(uint16 addr);

#endif
