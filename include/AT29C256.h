/*
 * =====================================================================================
 *
 *       Filename:  AT29C256.h
 *
 *    Description:  `
 *
 *        Version:  1.0
 *        Created:  2010-2-2 6:36:52
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */


#ifndef  AT29C256_H_INC
#define  AT29C256_H_INC

#include	"typedef.h"
void AT29C256_WrBuf(uint8 bufAddr,uint8 value);
uint8 AT29C256_WrPage ( bus_addr_t baseAddr,bus_addr_t addr );

#endif   /* ----- #ifndef AT29C256_H_INC  ----- */
