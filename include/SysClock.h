/*
 * =====================================================================================
 *
 *       Filename:  SysClock.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-8 14:02:46
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

void SysClock_Init();
