/*
 * =====================================================================================
 *
 *       Filename:  serial.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-14 14:55:23
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  SERIAL_H_INC
#define  SERIAL_H_INC
#include "typedef.h"
void Serial_Init();
void Serial_Send(uint8 sendChar)reentrant;
uint8 Serial_Recive(uint8 *readChar);               /* 未完成 */
void Serial_SendArr(uint8 buf[],uint8 len);               /* 未完成 */
uint8 Serial_ChkNoEmpty();               /* 未完成 */

#endif   /* ----- #ifndef SERIAL_H_INC  ----- */
