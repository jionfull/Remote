/*
 * =====================================================================================
 *
 *       Filename:  Fifo.h
 *
 *    Description:  Fifo 结构定义，函数声明
 *
 *        Version:  1.0
 *        Created:  2009-12-29 17:49:21
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */



#ifndef  FIFO_H_INC
#define  FIFO_H_INC
#include "typedef.h"


/*-----------------------------------------------------------------------------
 *FiFo 结构定义  
 *-----------------------------------------------------------------------------*/
typedef struct StruFifo{
//#ifdef __C51__
//						unsigned char data *buf;
//#else
					    uint8  *buf;
//#endif
						uint8 rdPoint; 
						uint8 wrPoint; 
						uint8 len;    
						uint8 flag;    
						} Fifo;

//#ifdef __C51__

//void Fifo_Init ( Fifo data *fifo,unsigned char data *buf,unsigned char len);
//unsigned char Fifo_CheckNoEmpty ( Fifo data *fifo );
//unsigned char Fifo_CheckFull (Fifo data *fifo );
//unsigned char  Fifo_ReadFifo (Fifo data *fifo,unsigned char data *rdChar);
//void Fifo_WriteFifo ( Fifo data *fifo,unsigned char wrChar);
//#else
void Fifo_Init (Fifo *fifo,unsigned char *buf,unsigned char len);
unsigned char Fifo_CheckNoEmpty ( Fifo *fifo );
unsigned char Fifo_CheckFull (Fifo *fifo );
unsigned char  Fifo_ReadFifo (Fifo *fifo,unsigned char *rdChar)  reentrant ;
void Fifo_WriteFifo (Fifo *fifo,unsigned char wrChar)  reentrant ;
//#endif
#endif  
