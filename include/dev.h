/*
 * =====================================================================================
 *
 *       Filename:  dev.h
 *
 *    Description:   硬件操作函数声明 函数实现在 arch\相关cpu\ 中  
 *
 *        Version:  1.0
 *        Created:  2009-12-30 10:23:27
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */
#include  "Fifo.h"
extern unsigned char serialTxBuf[];
extern unsigned char serialRxBuf[];
extern Fifo serialFilo;
void SerialSend(unsigned char sendChar);
unsigned char SerialRecive(unsigned char *readChar);               /* 未完成 */
void Gpio_Out(unsigned char portNumber,unsigned char outDate);
void Gpio_In(unsigned char portNumber,unsigned char *inDate);      /* 未完成 */
void Gpio_ClrBit(unsigned char portNumber,unsigned char clrBit);  
void Gpio_SetBit(unsigned char portNumber,unsigned char setBit); 
void Gpio_XorBit(unsigned char portNumber,unsigned char xorBit); 
