/*
 * =====================================================================================
 *
 *       Filename:  interrupt.h
 *
 *    Description: 通用中断处理声明, 函数在app\interrupt.c 中实现,不需要的在函数内不做处理
 *    		在arch\cpu 类型\hard_interrupt.c中 ,   做完硬件处理后调用,不需要时可不调用
 *
 *        Version:  1.0
 *        Created:  2009-12-28 17:44:28
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  _INTERRUPT_H_INC
#define  _INTERRUPT_H_INC

void App_Timer0Interrupt();
void App_SendEmptyOpt();
//void App_SerialTxInterrupt();
//void App_SerialRxInterrupt();

#endif   /* ----- #ifndef _INTERRUPT_H_INC  ----- */

