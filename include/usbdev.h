/*
 * =====================================================================================
 *
 *       Filename:  usbdev.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-2-11 18:55:23
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  USBDEV_INC
#define  USBDEV_INC
#include "typedef.h"
void USBDev_Init();
void USBDev_WrCmd(uint8 cmd);
uint8 USBDev_RdData(void);
void USBDev_RdID(uint8 id[]);
void USBDev_WrData(uint8 Value);
uint8 USBDev_RdEndptLastStatus(uint8 endpt);
void USBDev_AckSetup ( );
uint8 USBDev_RdEndptBuf ( uint8 endpt,uint8 len, uint8 *buf );
uint8 USBDev_WrEPBuf(uint8 endPt,uint8 Len,uint8 Buf[] ) reentrant;
uint8 USBDev_ClrBuf();
void USBDev_IntEn();
uint8 USBDev_RdIntReg ( );
uint8 USBDev_SetAddr(uint8 addr);
void USBDev_SetEpEn( );
#endif   /* ----- #ifndef USBDEV_INC  ----- */
