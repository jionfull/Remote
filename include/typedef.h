/*
 * =====================================================================================
 *
 *       Filename:  typedef.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-30 8:05:41
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  _TYPE_H_INC
#define  _TYPE_H_INC

#ifdef  __C51__

#define uint8    unsigned char 
#define uint16   unsigned short int   
#define uint32   unsigned long int  
#define int8     signed char  
#define int16    signed short int  
#define int32    signed long int  
#define uint64   unsigned long long int  
#define int64    signed long long int 
#define bus_data_t uint8
#define bus_addr_t uint16

#else      /* -----  not __C51__  ----- */
#define uint8    unsigned char 
#define uint16   unsigned short int 
#define uint32   unsigned long int
#define int8     signed char
#define int16    signed short int
#define int32    signed long int
#define uint64   unsigned long long int
#define int64    signed long long int
#define idata  
#define xdata 
#define data
#define pdata
#define reentrant
#define bus_data_t uint8
#define bus_addr_t uint16
#endif     /* -----  not __C51__  ----- */
typedef struct CmdBuf
{ 
	uint8  buf[16];
	uint8  len;
}StruCmdBuf;

typedef uint16 ticks_t; 

#endif   /* ----- #ifndef _TYPE_H_INC  ----- */
