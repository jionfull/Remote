/*
 * =====================================================================================
 *
 *       Filename:  Gpio.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-8 11:00:03
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  GPIO_h_INC
#define  GPIO_h_INC
void Gpio_Init();
void Gpio_Out(unsigned char portNumber,unsigned char outDate);
void Gpio_In(unsigned char portNumber,unsigned char *inDate);  
void Gpio_ClrBit(unsigned char portNumber,unsigned char clrBit);  
void Gpio_SetBit(unsigned char portNumber,unsigned char setBit); 
void Gpio_XorBit(unsigned char portNumber,unsigned char xorBit); 
#endif   /* ----- #ifndef GPIO_h_INC  ----- */
