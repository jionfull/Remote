/*
 * =====================================================================================
 *
 *       Filename:  remote.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-7-21 15:57:42
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  REMOTE_INC
#define  REMOTE_INC

void Remote_Init();
void Remote_Send(uint16 funNum);
void Remote_Learn(uint8 funNum);
uint8 Remote_ReadLearnState();
uint8 Remote_RdState();

#endif   /* ----- #ifndef REMOTE_INC  ----- */

