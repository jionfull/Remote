/*
 * =====================================================================================
 *
 *       Filename:  inithard.h
 *
 *    Description:  init hard
 *
 *        Version:  0.01
 *        Created:  2009-12-24 21:25:23
 *       Revision:  0.02 Add InitGpio();
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  INITHARD_INC
#define  INITHARD_INC

void InitGpio();
void  InitSerial();
void InitTimer0();
void EnInterrupt (  );

#endif   /* ----- #ifndef INITHARD_INC  ----- */
