/*
 * notie: vim set fdm=marker
 *file head{{{1
 * =====================================================================================
 *
 *       Filename:  RealTime.C
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  2010-1-7 15:25:54
 *       Revision:  none
 *       Compiler:
 *       	state:  LINE 204 需核对
 *
 *         Author:  寮犳櫙绂? (jionfull), jionfull@163.com
 *        Company:
 *
 * =====================================================================================
 * }}}1
 */

//include{{{1
#include	"..\include\bitopt.h"
#include	"..\include\typedef.h"
#include	"..\include\serial.h"
#include	"..\sys\GlobalVariable.h"
#include	"..\lib\crc.h"
#include        "..\include\Gpio.h"
#include        "..\include\remote.h"
#include        "..\include\I2C.h"
#include        "led.h"
//}}}1

//outher inclue{{{1
#include	"cfg.h"
#include	"MODBUS.h"
//}}}1

//{{{1
#define CMDRX_MAX 128
#define CMDTX_MAX 128
//}}}1

//数据位置定义{{{1
#define CMD_LOCALADDR	0x00
#define CMD_FUN			0x01
#define	CMD_REGADDRH	0x02		/*  */
#define	CMD_REGADDRL	0x03		/*  */
#define	CMD_NUMH		0x04 		/*  */
#define	CMD_NUML		0x05		/*  */
#define	CMD_DATAH		0x04 		/*  */
#define	CMD_DATAL		0x05		/*  */
#define CMD_F08SUBFUNH        0x02
#define CMD_F08SUBFUNL        0x03 


//}}}1

//功能编号定义{{{1
#define	FUN_RDSTATE 0x03 /*  */
#define	FUN_WRSTATE 0x06 /*  */
#define	FUN_RDIN 0x04 /*  */
#define	FUN_XUEXI	 0x08 /*  */
#define	FUN_RDFILE	 0x0E /*  */
#define	FUN_WRFILE	 0x0F /*  */
//}}}1

//功能编号定义接收长度定义{{{1
#define	FUN_RDSTATE_LEN	 0x08 /*  */
#define	FUN_WRSTATE_LEN	 0x08 /*  */
#define	FUN_XUEXI_LEN	 0x08 /*  */
#define	FUN_RDFILE_LINE	 0x09 /*  */
#define	FUN_WRFILE_LINE	 1+1+1+2+2+2+64+crc /* 暂时只响应64byte请求 */
//}}}1
//}}}1

//状态定义{{{1
#define	STATE_START 0x01 /*  */
//}}}1

static uint8 rxBuf[CMDRX_MAX];
static uint8 txBuf[CMDTX_MAX];
static uint8 rxLen;


/* {{{1
 * ===  FUNCTION  ======================================================================
 *         Name:  MODBUS_VarInit
 *  Description:
 * =====================================================================================
 }}}1 */
void MODBUS_VarInit()//{{{1
{
    txBuf[CMD_LOCALADDR]=Cfg_RdLocalAddr();
}//}}}1

/* {{{1
 * ===  FUNCTION  ======================================================================
 *         Name:  MODBUS_SendFunErr
 *  Description:
 * =====================================================================================
 }}}1*/
void MODBUS_SendFunErr(uint8 errCode)//{{{1
{
    uint16 result;
    txBuf[1]&=0x80;
    txBuf[2]=errCode;
    result=CrcCreate(txBuf,3);
    txBuf[3]=result&0xff;
    txBuf[4]=(result>>8)&0xff;
    Serial_SendArr(txBuf,5);
}
//}}}1

void  MODBUSRx_Task()
{
	
    //静态变量定义{{{1
    static  ticks_t currTicks=0;

    static uint8 rxPoint=0;

    static uint8 state;

    //}}}1

    //变量定义{{{2
    uint16 result;
    uint16 temp;
    uint8 rdChar;
    uint8 i;
    //}}}2

    if(Serial_ChkNoEmpty())
    {
        currTicks = ticks;
        Serial_Recive(&rdChar);
	
        if(CHKBIT(state,STATE_START))//检测是否为一新命令起始{{{1
        {
            if(rdChar == txBuf[CMD_LOCALADDR])//判断是否为本机地址
            {
                rxPoint=0;
                rxBuf[rxPoint++]=rdChar;
            }
            CLRBIT(state,STATE_START);
            return;
        }
        //}}}1

        if(rxPoint == 1)//判断搜到的是否为功能号并确定该功能的数据长度{{{1
        {
            rxBuf[rxPoint++]=rdChar;
            txBuf[CMD_FUN]=rdChar;
            switch(rdChar)
            {
            case FUN_RDSTATE:
                rxLen=FUN_RDSTATE_LEN;
                break;
            case FUN_WRSTATE:
                rxLen=FUN_WRSTATE_LEN;
                break;
            case FUN_RDIN:
                rxLen=FUN_RDSTATE_LEN;
                break;
            case FUN_XUEXI:
                rxLen=FUN_XUEXI_LEN;
                break;
            case FUN_RDFILE:
                rxLen=8;
                break;
            case FUN_WRFILE:
                rxLen=8;
                break;
            default:
                MODBUS_SendFunErr(0x01);
                break;
            }
            return;
        }
        //}}}1
	
        if(rxPoint == 6)//判断搜到的是否为功能号并确定该功能的数据长度{{{1
        {
            //rxBuf[rxPoint++]=rdChar;
            switch(rxBuf[1])
            {
            case FUN_WRFILE:
                rxLen=rdChar+9;//rdChar+4;
                break;
            default:
                //donothing
                break;
            }
            //return;
        }
        //}}}1

        if((rxPoint>1)&&(rxPoint<rxLen))//判断当前数据为本机数据{{{1
        {
            rxBuf[rxPoint++] = rdChar;
        }
        //}}}1


        if(rxPoint==rxLen)
        {
            rxPoint=0;
            
            result=CrcCreate(rxBuf,rxLen-2);
            temp=rxBuf[rxLen-1];
            temp=temp<<8;
            temp+=rxBuf[rxLen-2];
            if(result == temp)//CRC校验成功处理{{{1
            {
		//Led_Work(500);
                switch(rxBuf[CMD_FUN])//功能请求判断
                {
                case FUN_RDSTATE://读状态命令{{{2
                    txBuf[2]=rxBuf[CMD_NUML]*2;
                    for(i=0; i<rxBuf[CMD_NUML]; i++)
                    {
                        result=MODBUS_RdState(rxBuf[CMD_REGADDRL]+i);
                        txBuf[i*2+3]=result>>8&0xff;
                        txBuf[i*2+4]=result&0xff;
                    }
                    result=CrcCreate(txBuf,txBuf[2]+3);
                    txBuf[txBuf[2]+3]=result&0xff;
                    txBuf[txBuf[2]+3+1]=result>>8&0xff;
                    Serial_SendArr(txBuf,txBuf[2]+5);
                    break;//}}}2

                case FUN_WRSTATE://写状态命令{{{2
                    for(i=2; i<rxLen; i++)
                    {
                        txBuf[i]=rxBuf[i];

                    }
                    Serial_SendArr(txBuf,FUN_WRSTATE_LEN);
                    MODBUS_WrState(rxBuf[CMD_REGADDRL],(((uint16)rxBuf[CMD_DATAH]<<8)+rxBuf[CMD_DATAL]));
		    if(rxBuf[CMD_REGADDRL]==0)
		    {
			    txBuf[CMD_LOCALADDR]=rxBuf[CMD_DATAL];
		    }
			/*
                    switch ( rxBuf[CMD_REGADDRL] )//命令发送处理{{{3
                    {
                    case 1:
                        Remote_Send(rxBuf[CMD_DATAL]);
                        break;

                    case 2:
                        Remote_Send(rxBuf[CMD_DATAL]);
                        break;

                    case 3:
                        result=MODBUS_RdState(0x04);
                        if(result==0x01)//制冷发送{{{4
                        {

                            Remote_Send((MODBUS_RdState(0x03)-18)<<2+MODBUS_RdState(0x05)<<1+MODBUS_RdState(0x06)+4);//核对？
                        }//}}}4
                        if(result==0x02)//制热发送{{{4
                        {
                            Remote_Send((MODBUS_RdState(0x03)-18)<<2+MODBUS_RdState(0x05)<<1+MODBUS_RdState(0x06)+0x28);//核对？
                        }//}}}4
						
                        break;

                    case 4:
                        result=MODBUS_RdState(0x04);
                        if(result==0x01)//制冷发送{{{4
                        {
                            Remote_Send((MODBUS_RdState(0x03)-18)<<2+MODBUS_RdState(0x05)<<1+MODBUS_RdState(0x06)+4);//核对？
                        }//}}}4
                        if(result==0x02)//制热发送{{{4
                        {
                            Remote_Send((MODBUS_RdState(0x03)-18)<<1+MODBUS_RdState(0x05)<<1+MODBUS_RdState(0x06)+0x28);//核对？
                        }//}}}4
                        if(result==0x00)//自动命令发送{{{4
                        {
                            Remote_Send(0x03);
                        }///}}}4
                        if(result==0x04)//除湿命令发送{{{4
                        {
                            Remote_Send(0x04);
                        }///}}}4
                        break;

                    default:
                        break;
                    }
*/
                    break;//}}}2

                case FUN_RDIN://读输入命令{{{2
                    txBuf[2]=rxBuf[CMD_NUML]*2;
                    for(i=0; i<rxBuf[CMD_NUML]; i++)
                    {
                        result=(int16)(MODBUS_RdIn(rxBuf[CMD_REGADDRL]+i));
                        txBuf[i*2+3]=result>>8&0xff;
                        txBuf[i*2+4]=result&0xff;
                    }
                    result=CrcCreate(txBuf,txBuf[2]+3);
                    txBuf[txBuf[2]+3]=result&0xff;
                    txBuf[txBuf[2]+3+1]=result>>8&0xff;
                    Serial_SendArr(txBuf,txBuf[2]+5);
                    break;//}}}2

                case FUN_XUEXI://学习命令，需调试{{{2
                    for(i=2; i<FUN_WRSTATE_LEN; i++)
                    {
                        txBuf[i]=rxBuf[i];
                    }
                    if(rxBuf[CMD_REGADDRL]==0x01)//收到学习启动命令{{{3
                    {
                        Remote_Learn(rxBuf[CMD_DATAL]);
                        Serial_SendArr(txBuf,FUN_XUEXI_LEN);
                    }//}}}3
                    else//收到状态查询命令{{{3
                    {
                        //待调试
                        if(rxBuf[CMD_REGADDRL]==0x02)
                        {
                            txBuf[CMD_NUML]=Remote_ReadLearnState();
                             result=CrcCreate(txBuf,6);
                             txBuf[6]=result&0xff;
                             txBuf[7]=result>>8&0xff;
                             Serial_SendArr(txBuf,FUN_XUEXI_LEN);
                        }
                    }//}}}3
                    break;//}}}2

                case FUN_RDFILE://读文件命令，不兼容bochuang，制定上位机{{{2
		    txBuf[2]=rxBuf[CMD_NUML]*2;
                    Serial_SendArr(txBuf,3);
                    for(i=0; i<rxBuf[CMD_NUML]; i++)
                    {
                        result=MODBUS_RdFile(((uint16)rxBuf[CMD_REGADDRH]<<8)+rxBuf[CMD_REGADDRL]+i);
                        txBuf[i*2+3]=result&0xff;
                        Serial_Send(txBuf[i*2+3]);
                        txBuf[i*2+4]=result>>8&0xff;
                        Serial_Send(txBuf[i*2+4]);
                    }
                    result=CrcCreate(txBuf,txBuf[2]+3);
                    txBuf[txBuf[2]+3]=result&0xff;
                    Serial_Send(txBuf[txBuf[2]+3]);
                    txBuf[txBuf[2]+3+1]=result>>8&0xff;
                    Serial_Send(txBuf[txBuf[2]+3+1]);
                    //Serial_SendArr(txBuf,txBuf[2]+5);
                   break;//}}}2
		  
                case FUN_WRFILE://写文件命令，暂时未做,不兼容bochuang，制定上位机{{{2
                    for(i=2; i<6; i++)
                    {
                        txBuf[i]=rxBuf[i];
                    }
                    result=CrcCreate(txBuf,6);
                    txBuf[6]=result&0xff;
                    txBuf[7]=result>>8&0xff;
                    Serial_SendArr(txBuf,8);
                    uint16 tempAddr;
                    tempAddr=((uint16)rxBuf[CMD_REGADDRH]<<8)+rxBuf[CMD_REGADDRL];
                    MODBUS_WrFile(tempAddr,&rxBuf[7],rxBuf[6]);
                    break;//}}}2

                }

            }//}}}1
        }
    }
    else
    {

	
        if(ticks-currTicks>3)
        {
            currTicks=ticks;
            rxPoint=0;
            SETBIT(state,STATE_START);
        }
    }
}
