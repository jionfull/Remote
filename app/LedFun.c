/*
 * =====================================================================================
 *
 *       Filename:  AppFun.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2009-12-30 17:42:33
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#include "AppVariable.h"
#include	"..\include\dev.h"
#include	"config.h"
#include	"ZhiKu.h"
#include "..\include\bitopt.h"

void InitLedVariable (  )
{
	colCount=0;
        displayedTimes=0;
	shiftBits=0;
        SETBIT(ledFlag,NewLine);
}		/* -----  end of function   ----- */

void RowOut (unsigned char colNum )
{
/*
	unsigned char temp;
	if(shiftBits<8)
	{
		temp = rowChar[colNum][1]<<shiftBits;
                temp += rowChar[colNum][0]>>(8-shiftBits);
	SerialSend(~temp);
		temp = rowChar[colNum][0]<<shiftBits;
                temp += rowChar[colNum][1]>>(8-shiftBits);
	SerialSend(~temp);
	}
	else
	{
		temp = rowChar[colNum][0]<<shiftBits-8;
                temp += rowChar[colNum][1]>>(16-shiftBits);
	SerialSend(~temp);
		temp = rowChar[colNum][1]<<shiftBits;
                temp += rowChar[colNum][0]>>(16-shiftBits);
	SerialSend(~temp);
	}
*/
	SerialSend(~rowChar[(colNum+shiftBits)&0x0f][1]);
	SerialSend(~rowChar[(colNum+shiftBits)&0x0f][0]);
	return ;
}		/* -----  end of function   ----- */
void ColOut (unsigned char colNum )
{
	Gpio_Out(PortCtrl,colCtl[colNum]);
	return;
}		/* -----  end of function ColOut  ----- */
void LedOff (  )
{
	Gpio_SetBit(PortCtrl,LEDPOWER);
}		/* -----  end of function LedOff();  ----- */

void Ctrl_594_out()
{
  Gpio_ClrBit(PortCtrl,LOAD594);
  Gpio_SetBit(PortCtrl,LOAD594);
  Gpio_ClrBit(PortCtrl,LOAD594);
}

/*-----------------------------------------------------------------------------
 *  开led
 *-----------------------------------------------------------------------------*/
void LedOn (  )
{
	Gpio_ClrBit(PortCtrl,LEDPOWER);
}		/* -----  end of function LedOn  ----- */
