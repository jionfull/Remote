/*
 * =====================================================================================
 *
 *       Filename:  led.h
 *
 *    Description:  
 *        Version:  1.0
 *        Created:  2010-9-30 10:27:29
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  LED_H_INC
#define  LED_H_INC

#include "../include/typedef.h"			/*  */
void Led_Init();
void Led_Task();
void Led_Work(uint16 t);

#endif   /* ----- #ifndef LED_H_INC  ----- */

