/*
 * =====================================================================================
 *
 *       Filename:  MODBUS.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-7-6 13:36:39
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  寮犳櫙绂? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */


#ifndef  modbus_h_INC
#define  modbus_h_INC
#include "..\include\typedef.h"
/* 寄存器索命{{{1
0002  运行状态  0:关机状态，1:开机状态 
0003  温度设置值  制冷温度 18-26，表示18-26℃，制热温度 18/20，表示 18℃/20℃
0004  运行模式  0：自动，1：制冷，2：制热，3：去湿 
0005  扫风  0：扫风，1：不扫风 
0006  风速  0：微风，1：强风 
 * =====================================================================================
}}}1 */
typedef struct  __RmtRegs{
    uint16 lastCmd;
    uint16 powerState;
    uint16 temp;
    uint16 runMode;
    uint16 windScan;
    uint16 windSpeed;
}RmtRegs;

void   MODBUS_Init();
uint16 MODBUS_RdState(uint16 addr);
void MODBUS_WrState(uint16 addr,uint16 val);
void MODBUS_WrFile(uint16 addr,uint8 *src,uint8 len);//写文件记录
uint16 MODBUS_RdFile(uint16 addr);//读文件记录
uint16 MODBUS_RdIn(uint16 addr);

#endif   /* ----- #ifndef modbus_h_INC  ----- */
