/* 文件说明{{{1
 * vim: set fdm=marker
 * =====================================================================================
 *
 *       Filename:  ModBus.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-7-6 8:38:35
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  寮犳櫙绂? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 }}}1*/

/* 寄存器说明{{{1
0002  运行状态  0:关机状态，1:开机状态 
0003  温度设置值  制冷温度 18-26，表示18-26℃，制热温度 18/20，表示 18℃/20℃
0004  运行模式  0：自动，1：制冷，2：制热，3：去湿 
0005  扫风  0：扫风，1：不扫风 
0006  风速  0：微风，1：强风 
 * =====================================================================================
}}}1 */

//头文件包含{{{1
#include "..\include\typedef.h"
#include "..\include\Gpio.h"
#include "..\include\I2C.h"
#include "..\include\ad.h"
#include "..\include\bitopt.h"
#include "..\include\remote.h"
#include "modbus.h"
#include "cfg.h"
//}}}1

//静态变量定义{{{1
static RmtRegs  rmtRegs;
//}}}1

uint8 RmtRegs_Chk(RmtRegs * thiz)
{
   uint8 result;
    if(thiz->lastCmd>48)
    {
        thiz->lastCmd=0x00;
      result=1;
    }
    if(thiz->powerState>1)
    {
        thiz->powerState=0;
        result=1;
    }
    if(thiz->temp<18||thiz->temp>26)
    {
      thiz->temp=18;
       result=1;
    }
    if(thiz->runMode>3)
    {
        thiz->runMode=0;
        result=1;
    }
    if(thiz->windScan>1)
    {
        thiz->windScan=0;
         result=1;
    }
    if(thiz->windSpeed>1)
    {
        thiz->windSpeed=0;
         result=1;
    }
      return result;
}

void MODBUS_Init()//初始化{{{1
{
        I2C_ReadBytes(0xff02,(uint8 *)&rmtRegs,sizeof(rmtRegs));		
        if(RmtRegs_Chk(&rmtRegs))
        {
            I2C_WriteBytes(0xff02,(uint8 *)&rmtRegs,sizeof(rmtRegs));
            for(int i=0;i<10000;i++);
        }
}//}}}1

uint16 MODBUS_RdState(uint16 addr)//读保持寄存器{{{1
{
	return I2C_ReadWord(0xff00 + addr*2);
}//}}}1

uint16 MODBUS_RdIn(uint16 addr)//读输入寄存器{{{1
{
	uint16 result;
	switch(addr)
	{
		case 0:
			result=Ad_GetValue();
			break;
		case 3:
			result=SOFTVER;
			break;
		case 4:
			result=0xffff;
			break;
		default:
			result=0x0000;
			break;
			
	}
	return result;
}//}}}1

//WriteCmd{{{1
void WriteCmd(val)
{
	if(val>48)
	{
		return;
	}
	  if(val<4)//{{{3
	  {
		  rmtRegs.lastCmd=val;
		  switch(val)
		  {
				  case 0:
				        rmtRegs.powerState=0;
						  break;
				  case 1:
					rmtRegs.powerState=1;
						  break;
				  case 2:
					rmtRegs.runMode=0;
						  break;
				  case 3:
					rmtRegs.runMode=3;
						  break;
		  }
	  }
	  else
	  {
			if(CHKBIT(val,BIT0))//写扫风状态{{{4
			{
				rmtRegs.windScan=1;
			}
			else
			{
				rmtRegs.windScan=0;
			}//}}}4
			
			if(CHKBIT(val,BIT1))//写风速状态{{{4
			{
				rmtRegs.windSpeed=1;
			}
			else
			{
				rmtRegs.windSpeed=0;
			}//}}}4

	  }//}}}3

	  if(val>3&&val<40)//{{{3
	  {
		rmtRegs.temp=(val>>2)+18;
		rmtRegs.runMode=1;
	  }//}}}3
		  
	  if(val>=40&&val<48)//{{{3
	  {
		rmtRegs.temp=(val-40)/4;
		rmtRegs.temp*=2;
		rmtRegs.temp+=18;
		rmtRegs.runMode=1;
	  }//}}}3
	  I2C_WriteBytes(0xff02,(uint8 *)&rmtRegs,sizeof(rmtRegs));
	  Remote_Send(rmtRegs.lastCmd);
	  

}
//}}}1

void WritePowerState(val) //{{{1
{
	if(val>2)
	{
	}
	else
	{
		rmtRegs.powerState=val;
		rmtRegs.lastCmd=val;
	  	I2C_WriteBytes(0xff02,(uint8 *)&rmtRegs,sizeof(rmtRegs));
	 	Remote_Send(rmtRegs.lastCmd);
		if(rmtRegs.lastCmd==1)
		{
                while(Remote_RdState());
                for(uint16 i=0;i<10000;i++);
                switch(rmtRegs.runMode)//{{{4
				{
						case 0:
								rmtRegs.lastCmd=2;
								break;
						case 1:
								rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1)+rmtRegs.windSpeed+4;
								break;
						case 2:
								rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1) +rmtRegs.windSpeed+40;
								break;
						case 3:
								rmtRegs.lastCmd=3;
								break;
				}//}}}4
				I2C_WriteBytes(0xff02,(void *)&rmtRegs,sizeof(rmtRegs));
				Remote_Send(rmtRegs.lastCmd);
	}         
	}
	
}
//}}}1

void WriteTemp(val)//{{{1
{
	switch(rmtRegs.runMode)
	{
		case 0:
			break;
		case 1:
			if(val>17&&val<27)
			{
	       	             rmtRegs.temp=val;
			}
			else
			{
				rmtRegs.temp=18;
			}
			rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1)+rmtRegs.windSpeed+4;
            I2C_WriteBytes(0xff02,(void *)&rmtRegs,sizeof(rmtRegs));
			Remote_Send(rmtRegs.lastCmd);
			break;
		case 2:
			if(val==18||val==20)
			{
					rmtRegs.temp=val;
			}
			else
			{
					rmtRegs.temp=18;
			}
			rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1) +rmtRegs.windSpeed+40;
                        I2C_WriteBytes(0xff02,(void *)&rmtRegs,sizeof(rmtRegs));
			Remote_Send(rmtRegs.lastCmd);
			break;
		case 3:
			break;

	}
}//}}}1

void WriteRunMode(val)//{{{1
{
		if(val>4)
		{
				return;
		}
		else
		{
				rmtRegs.runMode=val;
				switch(val)//{{{4
				{
						case 0:
								rmtRegs.lastCmd=2;
								break;
						case 1:
								rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1)+rmtRegs.windSpeed+4;
								break;
						case 2:
								if(rmtRegs.temp!=18&&rmtRegs.temp!=20)
								{
										rmtRegs.temp=18;
								}
								rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1) +rmtRegs.windSpeed+40;
								break;
						case 3:
								rmtRegs.lastCmd=3;
								break;
				}//}}}4
				I2C_WriteBytes(0xff02,(uint8 *)&rmtRegs,sizeof(rmtRegs));
				Remote_Send(rmtRegs.lastCmd);
		}
		
}//}}}1

void WriteWindScan(val)//{{{1
{
		if(val>1)
		{
				return;
		}
	    else
		{
				rmtRegs.windScan=val;
				switch(rmtRegs.runMode)//{{{4
				{
						case 0:
								rmtRegs.lastCmd=2;
								break;
						case 1:
								rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1)+rmtRegs.windSpeed+4;
								break;
						case 2:
								rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1) +rmtRegs.windSpeed+40;
								break;
						case 3:
								rmtRegs.lastCmd=3;
								break;
				}//}}}4
				I2C_WriteBytes(0xff02,(void *)&rmtRegs,sizeof(rmtRegs));
				Remote_Send(rmtRegs.lastCmd);
		}
}//}}}1

void WriteWindSpeed(val)//{{{1
{
		if(val>1)
		{
				return;
		}
	    else
		{
				rmtRegs.windSpeed=val;
				switch(rmtRegs.runMode)//{{{4
				{
						case 0:
								rmtRegs.lastCmd=2;
								break;
						case 1:
								rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1)+rmtRegs.windSpeed+4;
								break;
						case 2:
								rmtRegs.lastCmd=((rmtRegs.temp-18)<<2)+(rmtRegs.windScan<<1) +rmtRegs.windSpeed+40;
								break;
						case 3:
								rmtRegs.lastCmd=3;
								break;
				}//}}}4
				I2C_WriteBytes(0xff02,(void *)&rmtRegs,sizeof(rmtRegs));
				Remote_Send(rmtRegs.lastCmd);
		}
}//}}}1

/*{{{1 
 * ===  FUNCTION  ======================================================================
 *         Name:  MODBUS_WrState
 *  Description:  
 * =====================================================================================
 }}}1*/
void MODBUS_WrState(uint16 addr, uint16 val)//{{{1
{
	int i;
	switch(addr)
	{
		case 0:
			I2C_WriteWord(0xff00 + addr*2,val); 
			for(i=0;i<20000;i++);
                        //(void *)0xfffe();	
			break;
	                	
		case 1:
			WriteCmd(val);
			break;
		case 2:
			WritePowerState(val);
			break;
		case 3:
			WriteTemp(val);
			break;
		case 4:
			WriteRunMode(val);
			break;
		case 5:
			WriteWindScan(val);
			break;
		case 6:
			WriteWindSpeed(val);
			break;
		default:
			I2C_WriteWord(0xff00 + addr*2,val); 
			break;

	}
      I2C_WriteWord(0xff00 + addr*2,val); 
	  if(addr==1)
	  {
		  if(val<4)//{{{3
		  {
			  switch(val)
			  {
					  case 0:
							  I2C_WriteWord(0xff00+2*2,0);
							  break;
					  case 1:
							  I2C_WriteWord(0xff00+2*2,1);
							  break;
					  case 2:
							  I2C_WriteWord(0xff00+4*2,0);
							  break;
					  case 3:
							  I2C_WriteWord(0xff00+4*2,3);
							  break;
			  }
		  }//}}}3

		  if(val>3&&val<40)//{{{3
		  {
				if(CHKBIT(val,BIT0))//写扫风状态{{{4
				{
						I2C_WriteWord(0xff00+6*2,1);
				}
				else
				{
						I2C_WriteWord(0xff00+6*2,0);
				}//}}}4
				
				if(CHKBIT(val,BIT1))//写风速状态{{{4
				{
						I2C_WriteWord(0xff00+5*2,1);
				}
				else
				{
						I2C_WriteWord(0xff00+5*2,0);
				}//}}}4

				I2C_WriteWord(0xff00+3*2,(val>>2)+18);//写温度值

				I2C_WriteWord(0xff00+4*2,1);//写运行状态值

		  }//}}}3
		  
		  if(val>=40&&val<48)//{{{3
		  {
				if(CHKBIT(val,BIT0))//写扫风状态{{{4
				{
						I2C_WriteWord(0xff00+6*2,1);
				}
				else
				{
						I2C_WriteWord(0xff00+6*2,0);
				}//}}}4
				
				if(CHKBIT(val,BIT1))//写风速状态{{{4
				{
						I2C_WriteWord(0xff00+5*2,1);
				}
				else
				{
						I2C_WriteWord(0xff00+5*2,0);
				}//}}}4

				I2C_WriteWord(0xff00+3*2,((val>>2)-40)*2+18);//写温度值

				I2C_WriteWord(0xff00+4*2,2);//写运行状态值

		  }//}}}3

	  }
}//}}}1

/*{{{1 
 * ===  FUNCTION  ======================================================================
 *         Name:  MODBUS_WrState
 *  Description:  
 * =====================================================================================
 }}}1*/
void MODBUS_WrFile(uint16 addr,uint8 *buf,uint8 len)//写文件记录{{{1
{
	I2C_WriteBytes(addr,buf,len);
}//}}}1

uint16  MODBUS_RdFile(uint16 addr)//读文件记录{{{1
{
	return I2C_ReadWord(addr*2);
}//}}}1
