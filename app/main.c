/*
 * =====================================================================================
 *
 *       Filename:  led.c
 *
 *    Description:  led dianzhen test
 *
 *        Version:  1.0
 *        Created:  2009-12-24 12:51:07
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:
 *
 * =====================================================================================
 */


/*-----------------------------------------------------------------------------
 *  头文件包�? *-----------------------------------------------------------------------------*/

#include "config.h"
#include "..\include\HardSys.h"
#include "..\include\Timer.h"
#include "..\include\Gpio.h"
#include "..\include\bitopt.h"
#include "..\include\serial.h"
#include "..\include\remote.h"
#include "..\include\I2C.h"
#include "..\include\ad.h"
#include "led.h"
#include	"rxCmd.h"
#include "MODBUS.h"



main (  )
{
	SysClock_Init();
        Gpio_Init();
	Timer0_Init();
	Serial_Init();
	Remote_Init();
	I2C_Init();
	Led_Init();
	Ad_Init();
	EnAllInterrupt();
        MODBUS_VarInit(); 
        MODBUS_Init();
	while(1)
	{
		MODBUSRx_Task();		
		Ad_Task();
		Led_Task();
	}
}				/* ----------  end of function main  ---------- */

