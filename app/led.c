/*
 * =====================================================================================
 *
 *       Filename:  led.c
 *
 *    Description:  led dianzhen test
 *
 *        Version:  1.0
 *        Created:  2009-12-24 12:51:07
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:
 *
 * =====================================================================================
 */
#include "..\include\typedef.h"
#include "..\sys\GlobalVariable.h"
#include "..\include\Gpio.h"
#define LEDPORT 0
#define WORKLED 0x08

static uint16 delayTime;
static  ticks_t currTicks=0;
void Led_Init()
{
	Gpio_ClrBit(LEDPORT,WORKLED);
	delayTime=0;
}

void Led_Task (  )
{
	

	if(delayTime!=0)
	{
               Gpio_SetBit(LEDPORT,WORKLED);
		if(ticks-currTicks>delayTime)
		{
			Gpio_ClrBit(LEDPORT,WORKLED);
			
			currTicks=ticks;
                        delayTime=0;
		}
	}
	else
	{
		currTicks=ticks;
	}
	
}		/* -----  end of function   ----- */
void Led_Work(uint16 t)
{
	delayTime=t;
	Gpio_SetBit(LEDPORT,WORKLED);
}		/* -----  end of function   ----- */
