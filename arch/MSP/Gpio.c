/*
 * =====================================================================================
 *
 *       Filename:  dev.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2009-12-30 11:06:36
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */
#include "MSP430x14x.h"
#include "config.h"
#include "..\..\include\bitopt.h"

void Gpio_Init (  )
{ 
   
   P1SEL = 0x00;                                /* 功能选择 */
   P1DIR = 0x00;                                /* 输入输出选择 */
   P1OUT = 0x00;
   
   
   P2SEL =0x00;
   P2DIR =0x00;
   P2OUT =0x02;
   
   P3SEL = 0xf0; 
   P3DIR = 0x00; 
   P3OUT = 0x21;
   
   P4SEL = 0x70; 
   P4DIR = 0x00; 
   P4OUT = 0x21;
   
   P5SEL = 0x00; 
   P5DIR = 0x00; 
   P5OUT = 0x21;

   P6SEL = 0xff;//0X41
   P6DIR = 0x00;
   P6OUT=0x00;
 
}		/* -----  end of function InitGpio  ----- */

void Gpio_Out(unsigned char portNumber,unsigned char outDate)
{
	switch(portNumber)
	{
		case 0:
			P1OUT=outDate;
			break;
		case 1:
			P2OUT=outDate;
			break;
		case 2:
			P3OUT=outDate;
			break;
		case 3:
			P4OUT=outDate;
			break;
		case 4:
			P5OUT=outDate;
			break;
		case 5:
			P6OUT=outDate;
			break;
	}
}

void Gpio_In(unsigned char portNumber,unsigned char *inData)      /* 未完�?*/
{
                                                /* 未完�?*/
	switch(portNumber)
	{
		case 0:
			*inData =  P1OUT ;
			break;
		case 1:
			*inData =  P2OUT ;
			break;
		case 2:
			*inData =  P3OUT ;
			break;
		case 3:
			*inData =  P4OUT ;
			break;
		case 4:
			*inData =  P5OUT ;
			break;
		case 5:
			*inData =  P6OUT ;
			break;
	}
}
void Gpio_ClrBit(unsigned char portNumber,unsigned char clrBit)
{
	switch(portNumber)
	{
		case 0:
			P1OUT &=~ clrBit;
			break;
		case 1:
			P2OUT &=~ clrBit;
			break;
		case 2:
			P3OUT &=~ clrBit;
			break;
		case 3:
			P4OUT &=~ clrBit;
			break;
		case 4:
			P5OUT &=~ clrBit;
			break;
		case 5:
			P6OUT &=~ clrBit;
			break;
	}
}
void Gpio_SetBit(unsigned char portNumber,unsigned char setBit)
{
	switch(portNumber)
	{
		case 0:
			P1OUT |= setBit;
			break;
		case 1:
			P2OUT |= setBit;
			break;
		case 2:
			P3OUT |= setBit;
			break;
		case 3:
			P4OUT |= setBit;
			break;
		case 4:
			P5OUT |= setBit;
			break;
		case 5:
			P6OUT |= setBit;
			break;
	}
}
void Gpio_XorBit(unsigned char portNumber,unsigned char xorBit)
{
	switch(portNumber)
	{
		case 0:
			P1OUT ^= xorBit;
			break;
		case 1:
			P2OUT ^= xorBit;
			break;
		case 2:
			P3OUT ^= xorBit;
			break;
		case 3:
			P4OUT ^= xorBit;
			break;
		case 4:
			P5OUT ^= xorBit;
			break;
		case 5:
			P6OUT ^= xorBit;
			break;
	}
}
