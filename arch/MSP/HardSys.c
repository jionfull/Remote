/*
 * =====================================================================================
 *
 *       Filename:  sysclock.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-8 13:55:24
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#include <msp430x14x.h> 

void SysClock_Init()
{

   WDTCTL = WDTPW + WDTHOLD;
   IFG1 = 0;
   IFG2 = 0;
   BCSCTL1 = 0x84;
   BCSCTL2 = 0;
   BCSCTL1 &= ~XT2OFF;
   _BIC_SR(OSCOFF);
   while(OFIFG & IFG1)
   {
      IFG1 &= ~OFIFG;
      _NOP();
      _NOP();
    }
   BCSCTL2 = 0x80;
   BCSCTL2 |= 8;
}
void EnAllInterrupt (  )
{
  	_EINT();
}
