/*
 * =====================================================================================
 *
 *       Filename:  timer.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-8 11:12:11
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#include <msp430x14x.h> 
#include "..\..\sys\GlobalVariable.h"
#include	"..\..\include\interrupt.h"

void Timer0_Init()
{
   CCR0 = 1000;  //设置计时时间
   TACTL = MC_2 +ID_3 + TASSEL_2 ;/*Timer_A set.up mode, divide by 8,
										   clock use the submain clock*/
   TACCTL0 = CCIE;
}


#pragma vector=TIMERA0_VECTOR
__interrupt void timer_a0(void)   
{
   CCR0+=1000;
	ticks++;
//      App_Timer0Interrupt();
}
