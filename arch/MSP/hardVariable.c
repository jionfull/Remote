/*
 * =====================================================================================
 *
 *       Filename:  hardVariable.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2009-12-30 11:31:19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#include	"config.h"
#include "..\..\include\Fifo.h"
#ifdef  TX_FIFO_ON
unsigned char serialTxBuf[TX_FIFO_MAX];
Fifo serialTxFifo;
unsigned char txState;
#endif     /* -----  not TX_FIFO_ON  ----- */

#ifdef  RX_FIFO_ON
unsigned char serialRxBuf[TX_FIFO_MAX];
Fifo serialRxFifo;
#endif     /* -----  not RX_FIFO_ON  ----- */

