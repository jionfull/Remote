/*
 * =====================================================================================
 *
 *       Filename:  ad.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-8-1 1:13:42
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#include "msp430x14x.h"
#include "..\..\include\typedef.h"
#include "..\..\include\bitopt.h"
#include "..\..\include\I2C.h"
#include "..\..\sys\GlobalVariable.h"

#define TEMPCH 0x0A
#define TASKPERIOD 1000

static int32 k;
static uint16 b;
static uint16 result;
static uint16 buf[4];


void Ad_Init()//{{{1
{

  /*Initialize ADC*/
  CLRBIT(ADC12CTL0,ADC12SC);/*Disenable ADC convert*/
   ADC12CTL0 = SHT1_12+SHT0_12+REFON;
  ADC12CTL0 = SHT0_8 + REFON + ADC12ON;
  ADC12CTL1 = SHP;                          // enable sample timer
  ADC12MCTL0 = SREF_1 + INCH_10;
 
//  ADC12CTL1 = SHP;                          // Use sampling timer
  ADC12IE = 0x01;                           // Enable interrupt
  ADC12CTL0 |= ENC;                         // Conversion enabled

  
  SETBIT(ADC12CTL0,ADC12SC);//����ad�任
   k=I2C_ReadWord(0xff20);
   b=I2C_ReadWord(0xff22);
		
}//}}}1

void Ad_Start()//{{{1
{
		SETBIT(ADC12CTL0,ADC12SC);
}//}}}1

#pragma vector=ADC12_VECTOR
__interrupt void ADC12_ISR()//{{{1
{
  static uint8 i=0;
		//result -= buf[i];
		buf[i++&0x03]  =  ADC12MEM0;
		//result += buf[i-1];
}//}}}1

int16 Ad_GetValue()//{{{1
{   
   uint16 i;
   long temp;
         result=0;
        for(i=0;i<4;i++)
        {
          result+=buf[i];
        }
        
        temp=result;
  //      temp-=(2692<<2);
           temp-=b;
//        temp*=16920;
           temp*=k;
        temp>>=16;
        
        result=temp;
	return result;
}//}}}1

void Ad_Task()
{
	static  ticks_t currticks=0;
	if(ticks-currticks>TASKPERIOD)
	{
			currticks=ticks;
			Ad_Start();
 	}


}
