/*
 * =====================================================================================
 *
 *       Filename:  serial.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-14 14:27:03
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景�? (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#include <msp430x14x.h> 
#include "..\..\include\typedef.h"
#include "..\..\include\bitopt.h"
#include "..\..\include\fifo.h"
#include "config.h"
#include "..\..\include\interrupt.h"
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  InitSerial
 *  Description:  
 * =====================================================================================
 */
#ifdef  TX_FIFO_ON //{{{1
static  uint8  serialTxBuf[TX_FIFO_MAX];
static  Fifo  serialTxFifo;
static uint8 txState;
#define TXBUSY 0x01
#endif   //}}}1 

#ifdef  RX_FIFO_ON
static uint8 serialRxBuf[TX_FIFO_MAX];
static Fifo serialRxFifo;
#endif     /* -----  not RX_FIFO_ON  ----- */

void  Serial_Init()//串口初始化{{{1
{
  uint16 i;
/*fifo 初始化{{{2*/	
#ifdef TX_FIFO_ON
	Fifo_Init(&serialTxFifo,serialTxBuf,TX_FIFO_MAX);
#endif
#ifdef RX_FIFO_ON
	Fifo_Init(&serialRxFifo,serialRxBuf,RX_FIFO_MAX);
#endif


   UCTL0 = 0x10;//0X0D0  串口设置�?bit
   UTCTL0 =0x20;//Trans设置
   URCTL0 =0x00;//0x04 Recive 设置
   i=8000000/9600;
   UBR10=i/256;                      
   UBR00=i-UBR10*256;
   UMCTL0=0;
   ME1 |= URXE0;
   ME1 |= UTXE0;
   IE1 |= URXIE0;
   IE1 |= UTXIE0;

	
}//串口初始化}}}1


void Serial_Send(uint8 sendChar)//串口发送{{{1
{
	if(CHKBIT(txState,TXBUSY))
	{
		
		if(!Fifo_CheckFull(&serialTxFifo))
		{
			Fifo_WriteFifo(&serialTxFifo,sendChar);
		}
	}
	else
	{
		U0TXBUF=sendChar;
		SETBIT(txState,TXBUSY);
		txState=TXBUSY;
	}
}//串口发送}}}1

/*Des{{{1 
 * ===  FUNCTION  ======================================================================
 *         Name:  Serial_SendArr
 *  Description:  数组发�? * =====================================================================================
 }}}1*/
void Serial_SendArr(uint8 buf[],uint8 len)//{{{1
{
	uint8 i;
	for(i=0;i<len;i++)
	{
		Serial_Send(buf[i]);
	}
}//}}}1

/*Des{{{1 
 * ===  FUNCTION  ======================================================================
 *         Name:  Serial_Recive
 *  Description:  接受一个char,返回读取是否成功，返�?表示缓存为空，返�?表示成功读取一
 *  			个char
 * =====================================================================================
 }}}1*/
uint8 Serial_Recive(unsigned char *readChar)//{{{1
{
	return Fifo_ReadFifo(&serialRxFifo,readChar);

}//}}}1

uint8 Serial_ChkNoEmpty()//{{{1
{
		return Fifo_CheckNoEmpty(&serialRxFifo);
}//}}}1

/*Description{{{1 
 * ===  FUNCTION  ======================================================================
 *         Name:  SerialInterrupt
 *  Description:  UART0接收中断
 * =====================================================================================
 }}}1*/
#pragma vector=0x10
__interrupt void UTX_0(void)  //{{{1
{
		uint8 temp;
#ifdef TX_FIFO_ON 
		if(Fifo_CheckNoEmpty(&serialTxFifo))
		{
			Fifo_ReadFifo(&serialTxFifo,&temp);
			U0TXBUF=temp;
		}
		else
		{
			CLRBIT(txState,TXBUSY);
			App_SendEmptyOpt();
		}
#else
		App_SendEmptyOpt();
#endif
}//}}}1


#pragma vector=0x12
__interrupt void URX_0()//{{{1  
{
		uint8 temp;
		temp=U0RXBUF;
		Fifo_WriteFifo(&serialRxFifo,temp);
}//}}}1
