/*
 * =====================================================================================
 *
 *       Filename:  I2C_1.c
 *
 *    Description:  本程序改自   
 *    “ 孤独的牧羊人”http://tywood.blog.163.com/blog/static/171391052008111721015523/
 *
 *        Version:  1.0
 *        Created:  2010-7-14 23:24:21
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#include "msp430x14x.h"
#include "..\..\include\bitopt.h"
#include "..\..\include\typedef.h"

#define SDA_1       P3OUT |=  BIT3              //SDA = 1
#define SDA_0       P3OUT &=~ BIT3              //SDA = 0
#define SCL_1       P3OUT |=  BIT2              //SCL = 1
#define SCL_0       P3OUT &=~ BIT2              //SCL = 0
#define DIR_IN      P3DIR &=~ BIT3;  SDA_1      //I/O口为输入
#define DIR_OUT     P3DIR |=  BIT3              //I/0口为输出
#define SDA_IN      ((P3IN >> 3) & 0x01)        //Read SDA


static void Delay(uint8 n)
{
    uint8 i;
    for (i=0; i<n;i++)
        ;
}
        

void I2C_Init()
{
	SETBIT(P3DIR,BIT2+BIT3);
    SCL_1;
    Delay(8);
    SDA_1;
    Delay(8);
}

static void Start(void)
{
    SDA_1;
    Delay(8);
    SCL_1;
    Delay(8);
    SDA_0;
    Delay(8);
    SCL_0;
    Delay(8);
}

static void Stop(void)
{
    SDA_0;
    Delay(8);
    SCL_1;
    Delay(8);
    SDA_1;
    Delay(8);
}

static void WriteByte(uint8 WriteData)
{
    unsigned char i;
    
    for (i=0; i<8; i++)
    {
        SCL_0;
        Delay(8);
        
        if (((WriteData >> 7) & 0x01) == 0x01)
        {
            SDA_1;
        }
        else
        {
            SDA_0;
        }
        
        Delay(8);
        SCL_1;
        WriteData = WriteData << 1;
        Delay(8);
    }
    
    SCL_0;
    SDA_1;
    Delay(8);
    Delay(8);
}

uint8 ReadByte(void)
{
    uint8 i;
    uint8 TempBit  = 0;
    uint8 TempData = 0;
    
    SCL_0;
    Delay(8);
    SDA_1;
    for (i=0; i<8; i++)
    {
        Delay(8);
        SCL_1;
        Delay(8);
        DIR_IN;
        if (SDA_IN == 0x01 /*sda==1*/)
        {
            TempBit = 1;
        }
        else
        {
            TempBit = 0;
        }
        
        DIR_OUT;
        TempData = (TempData << 1) | TempBit;
        SCL_0;
    }
    
    Delay(5);
    return(TempData);
}

void ReceiveAck(void)
{
  uint8 i=0;
   
    SCL_1;
    Delay(8);
    DIR_IN;
    while ((SDA_IN == 0x01 /*sda==1*/) && (i < 255)/*调试方便,可以不要*/)
    {
        i++;
    }
    DIR_OUT;
    SCL_0;
    Delay(8);
}

void Acknowledge(void)
{
    SCL_0;
    Delay(8);
    DIR_OUT;
    SDA_0;
    SCL_1;
    Delay(8);
    SCL_0;
}

uint16 I2C_ReadWord(uint16 addr)
{
    uint16 result;
    Start();
    WriteByte(0xa0);
    ReceiveAck();
    WriteByte(HIGH(addr));
    ReceiveAck();
    WriteByte(LOW(addr));
    ReceiveAck();
    Start();
    WriteByte(0xa1);
    ReceiveAck();
    result = ReadByte();
    Acknowledge();
    result += ((uint16)ReadByte())<<8;
    Stop();
    return(result);
}

void I2C_ReadBytes(uint16 addr, uint8 *dst,uint8 len)//len <=  64(a page)
{
    unsigned char i;
    
    
    Start();
    WriteByte(0xa0);
    ReceiveAck();
    WriteByte(HIGH(addr));
    ReceiveAck();
    WriteByte(LOW(addr));
    ReceiveAck();
    Start();
    WriteByte(0xa1);
    ReceiveAck();
    
    for (i=0; i<len-1; i++)
    {
        dst[i]  = ReadByte();
        Acknowledge();
    }
     
    Stop();
 }

void I2C_WriteWord(uint16 addr, uint16 val)
{
    
    Start();
    WriteByte(0xa0);
    ReceiveAck();
    WriteByte(HIGH(addr));
    ReceiveAck();
    WriteByte(LOW(addr));
    ReceiveAck();
    WriteByte(LOW(val));
    ReceiveAck();
    WriteByte(HIGH(val));
    ReceiveAck();
    Stop();
}

void I2C_WriteBytes(uint16 addr, uint8 *dst,uint8 len)//len <=  64(a page)
{
	if(len>=128)
	{
		len=128;
	}
    uint8 i;
    Start();
    WriteByte(0xa0);
    ReceiveAck();
    WriteByte(HIGH(addr));
    ReceiveAck();
    WriteByte(LOW(addr));
    ReceiveAck();
	for(i=0;i<len;i++)
	{
          WriteByte(dst[i]);
	    ReceiveAck();
	}
    Stop();
}
