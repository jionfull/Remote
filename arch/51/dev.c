/*
 * =====================================================================================
 *
 *       Filename:  dev.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2009-12-30 11:06:36
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */
#include "Reg51.h"
#include "config.h"
#include "..\..\include\bitopt.h"
#include "..\..\include\Fifo.h"
#include	"hardVariable.h"

void SerialSend(unsigned char sendChar)
{
	if(CHKBIT(txState,TXBUSY))
	{
		if(!Fifo_CheckFull(&serialTxFifo))
		{
			Fifo_WriteFifo(&serialTxFifo,sendChar);
		}
	}
	else
	{
		SBUF=sendChar;
		SETBIT(txState,TXBUSY);
	}
}

/*-----------------------------------------------------------------------------
 *  
 *-----------------------------------------------------------------------------*/
unsigned char SerialRecive(unsigned char *readChar)               /* 未完成 */
{

}

/*-----------------------------------------------------------------------------
 *  
 *-----------------------------------------------------------------------------*/
void Gpio_Out(unsigned char portNumber,unsigned char outDate)
{
	switch(portNumber)
	{
		case 0:
			P0=outDate;
			break;
		case 1:
			P1=outDate;
			break;
		case 2:
			P2=outDate;
			break;
		case 3:
			P3=outDate;
			break;
	}
}
void Gpio_In(unsigned char portNumber,unsigned char *inDate)      /* 未完成 */
{
                                                /* 未完成 */
}
void Gpio_ClrBit(unsigned char portNumber,unsigned char clrBit)
{
	switch(portNumber)
	{
		case 0:
			P0 &=~ clrBit;
			break;
		case 1:
			P1 &=~ clrBit;
			break;
		case 2:
			P2 &=~ clrBit;
			break;
		case 3:
			P3 &=~ clrBit;
			break;
	}
}
void Gpio_SetBit(unsigned char portNumber,unsigned char setBit)
{
	switch(portNumber)
	{
		case 0:
			P0 |= setBit;
			break;
		case 1:
			P1 |= setBit;
			break;
		case 2:
			P2 |= setBit;
			break;
		case 3:
			P3 |= setBit;
			break;
	}
}
void Gpio_XorBit(unsigned char portNumber,unsigned char xorBit)
{
	switch(portNumber)
	{
		case 0:
			P0 ^= xorBit;
			break;
		case 1:
			P1 ^= xorBit;
			break;
		case 2:
			P2 ^= xorBit;
			break;
		case 3:
			P3 ^= xorBit;
			break;
	}
}
