/*
 * =====================================================================================
 *
 *       Filename:  arch\51\hard_interrupet.c
 *
 *    Description:  interrupet 函数,硬件相关部分,
 *                  做完处理后调用通用函数,声明在include\interrupt.h中
 *
 *        Version:  1.0
 *        Created:  2009-12-26 17:36:16
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#include	<reg51.h>
#include	"reg51bit.h"
#include	"..\..\include\interrupt.h"
#include 	"config.h"
#include	"..\..\include\bitopt.h"

/* 
 * ===  FUNCTION  ======================================================================
 *  Name:  InitTimer0
 *  Description:  
 * =====================================================================================
 */
void Timer0_Init()
{
	TH0 = (0xffff-DELAYTIME)>>8;
	TL0 = (0xffff-DELAYTIME)&&0xff;
	SETT0MOD(TMOD_MODE1);  
   	TR0 = 1 ;
	ET0 = 1;
}


void timer0Interrupt (  ) interrupt 1
{
    TH0 = (0xffff-DELAYTIME) >> 8;
   	TL0 = (0xffff-DELAYTIME) & 0xff;
	App_Timer0Interrupt();
}		/* -----  end of function   ----- */


