/*
 * =====================================================================================
 *
 *       Filename:  bus_opt.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-29 13:53:12
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */
#include "..\..\include\typedef.h"

bus_data_t bus_rd(bus_addr_t addr)
{
	return * ((unsigned char volatile xdata *) addr);
}
/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  bus_wr
 *  Description:  
 * =====================================================================================
 */
void bus_wr(bus_addr_t addr,bus_data_t value)
{
	* ((unsigned char volatile xdata *) addr) = value;
}
