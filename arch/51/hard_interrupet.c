/*
 * =====================================================================================
 *
 *       Filename:  arch\51\hard_interrupet.c
 *
 *    Description:  interrupet 函数,硬件相关部分,
 *                  做完处理后调用通用函数,声明在include\interrupt.h中
 *
 *        Version:  1.0
 *        Created:  2009-12-26 17:36:16
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#include	<Reg51.h>
#include	"Reg51bit.h"
#include	"..\..\include\interrupt.h"
#include	"..\..\include\Fifo.h"
#include 	"config.h"
#include	"hardVariable.h"
#include	"..\..\include\bitopt.h"



void serialInterrupt ( ) interrupt 4
{
	unsigned char temp;
	if(TI)
	{
		TI=0;
#ifdef TX_FIFO_ON 
		if(Fifo_CheckNoEmpty(&serialTxFifo))
		{
			Fifo_ReadFifo(&serialTxFifo,&temp);
			SBUF=temp;
		}
		else
		{
			CLRBIT(txState,TXBUSY);
			App_SendEmptyOpt();
		}
#else
		App_SendEmptyOpt();
#endif
	}
	if(RI)
	{
	 	RI=0;                           /* 未写完 */
	}
	return;
}		/* -----  end of function   ----- */
void timer0Interrupt (  ) interrupt 1
{
    	TH0 = (0xffff-DELAYTIME) >> 8;
   	TL0 = (0xffff-DELAYTIME) & 0xff;
	App_Timer0Interrupt();
}		/* -----  end of function   ----- */


