/*
 * =====================================================================================
 *
 *       Filename:  config.h
 *
 *    Description:  Ӳ������
 *
 *        Version:  1.0
 *        Created:  2009-12-25 6:47:40
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  _CONFIG_H_INC
#define  _CONFIG_H_INC

#define  DELAYTIME 1000 /**/

#define	 TX_FIFO_ON	        	/*  */
#define	RX_FIFO_ON	        	/*  */
#define	 TX_FIFO_MAX  32		/*  */
#define RX_FIFO_MAX  8
#define FOSC        (22118400UL)
#define BAUD        (19200UL)
#define SERMOD_UART8
#endif   /* ----- #ifndef _CONFIG_H_INC  ----- */

