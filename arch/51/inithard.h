/*
 * =====================================================================================
 *
 *       Filename:  inithard.h
 *
 *    Description:  init hard
 *
 *        Version:  1.0
 *        Created:  2009-12-24 21:25:23
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  INITHARD_INC
#define  INITHARD_INC

void  InitSerial();
void InitTimer0();
void EnInterrupt (  );

#endif   /* ----- #ifndef INITHARD_INC  ----- */
