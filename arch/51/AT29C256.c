/*
 * =====================================================================================
 *
 *       Filename:  AT29C256.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2010-1-31 8:37:50
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#include	<reg52.h>
#include	"..\..\include\typedef.h"
#include	"..\..\include\busopt.h"
#include	"..\..\include\bitopt.h"

static unsigned char idata AT29C256_Buf[64];
static unsigned char idata AT29C256_state;
#define wrBusy 0x01
#define WRITEERR 0xff
#define WRITESUCCESS 0xff
void AT29C256_WrBuf(uint8 bufFrmNo,uint8 value)
{
	AT29C256_Buf[bufFrmNo]=value;

}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  AT29C256_WrPage
 *  Description:  
 * =====================================================================================
 */
uint8 AT29C256_WrPage ( bus_addr_t baseAddr,bus_addr_t addr )
{
	uint8 i;
        if(CHKBIT(AT29C256_state,wrBusy))	
		{
		//��д
		return WRITEERR;
		}
	for ( i=0;i<64 ;i++ )
	bus_wr(baseAddr+addr+i,AT29C256_Buf[i]);
	return WRITESUCCESS;
	
}		/* -----  end of function AT29C256_WrPage  ----- */
