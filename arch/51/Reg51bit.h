/*define some 51 bits use the logic opt.  zjf  E_mail: jionfull@163.com
 *chinese:本头文件定义了一些位，方便用逻辑的方式操作寄存器，
            使操作意图比较明显，借鉴了IAR的一些做法
            作者：张景福，E_Mail:jionfull@163.com

 *editor:CodeBlock
 *$Id: G_Reg51.h,v 1.0 2009-03-02 06:54:09+08 jionfull Exp jionfull $
 *$Log: G_Reg51.h,v $
 *Revision 1.0  2009-03-02 06:54:09+08  jionfull
 *Initial revision
 *
 *Note:if you change the code please mail to me & tell me why.
 */
#ifndef G_REG51_H_INCLUDED
#define G_REG51_H_INCLUDED

/* psw bits define */
#define BIT_CY   0x80
#define BIT_AC   0x40
#define BIT_F0   0x20
#define BIT_RS1  0x10       //(R)egister (S)elect
#define BIT_RS0  0x08
#define RS_MODE0 0x00*0x08
#define RS_MODE1 0x01*0x08
#define RS_MODE2 0x02*0x08
#define RS_MODE3 0x03*0x08
#define BIT_OV   0x04
#define BIT_P    0x01


/*
 *TCON bits define TCON is Timer control
 */
#define BIT_TF1 0x80 //Timer1 overflow Flag
#define BIT_TR1 0x40 //Timer1 Restart Flag
#define BIT_TIMER1_RUN 0x40 //Timer1 运行开始
#define BIT_TF0 0x20 //Timer0 Overflow Flag
#define BIT_TR0 0x10 //Timer0 Restart Flag
#define BIT_TIMER0_RUN 0x10 //Timer0 运行开始
#define BIT_IE1 0x08 //External Interrupt 1 Edge flag,
                     //set by hardware,clear by hardware is processed
#define BIT_IT1 0x04 //Externel Interrupt 1 Type flag
                     //Set to falling edge, clr to low level
#define BIT_IE0 0x02 //External Interrupt 0 Edge flag,
                     //
#define BIT_IT0 0x01 //Externel Interrupt 0 Type flay,


/*
 *TMOD (T)ime (mod)e
 */
#define T1MOD_CLR   0x0F //
#define T0MOD_CLR   0xF0 //
#define BIT_T1GATE  0x80 //
#define BIT_T0GATE  0x08 //
#define BIT_T1CT    0x40 // (C)ounter or (T)imer, clr is Timer
#define BIT_T0CT    0x04 //
#define TMOD_MODE0 0x00 //
#define TMOD_MODE1 0x01 //ReLoad Mode
#define TMOD_MODE2 0x02 //
#define TMOD_MODE3 0x03 //

/*
 *Reg IE
 */

#define BIT_EA   0x80 //(E)nable  (A)ll interrupt,clr To disable all Interrupt
#define BIT_INT_ENALL   0x80 //(E)nable  (A)ll interrupt,clr To disable all Interrupt
#define BIT_INT_ENT2  0x20 //(E)nable  (T)imer(2) Interrupt
#define BIT_ET2  0x20 //(E)nable  (T)imer(2) Interrupt
#define BIT_ES   0x10 //(E)nable  (S)erial port interrupt
#define BIT_ET1  0x08 //(E)nable  (T)imer(1) interrupt
#define BIT_EX1  0x04 //(E)nable  E(x)ternal(1) interrupt
#define BIT_ET0  0x02 //(E)nable  (T)imer(0) interrupt
#define BIT_EX0  0x01 //(E)nable  E(x)ternal(0) interrupt
#define BIT_INT_ENSER   0x10 //(E)nable  (S)erial port interrupt
#define BIT_INT_ENT1  0x08 //(E)nable  (T)imer(1) interrupt
#define BIT_INT_ENEX1  0x04 //(E)nable  E(x)ternal(1) interrupt
#define BIT_INT_ENT0  0x02 //(E)nable  (T)imer(0) interrupt
#define BIT_INT_ENEX0  0x01 //(E)nable  E(x)ternal(0) interrupt




/*
 * IP (I)nerrupt (P)riority
 */

#define BIT_PT2  0x20 //(P)riority of (T)imer(2)
#define BIT_PS   0x10 //(P)riority of (S)erial
#define BIT_PT1  0x08 //(P)riority of (T)imer(1)
#define BIT_PX1  0x04 //(P)riority of E(x)ternal(1)
#define BIT_PT0  0x02 //(P)riority of (T)imer(0)
#define BIT_PX0  0x01 //(P)riority of E(x)ternal(0)

/*
 *SCON (S)erial (con)trol
 */

#define BIT_SM0        0x80       //(S)erial (M)ode bit  0,best to use SM_MODEx
#define BIT_SM1        0x40       //(S)erial (M)ode bit  1,best to use SM_MODEx
#define SM_MODE0       0x00*0x40  //Shift Mode Baud rate =Fosc/12
#define SM_SHIFT       0x00*0x40  //see the Mode0
#define SM_MODE1       0x01*0x40  //8 bit variable Uart.
#define SM_UART_BIT8   0x01*0x40  //see the Mode1.
#define SM_MODE2       0x02*0x40  //9 bit Uart,Baud = Fosc/64 Fosc/32.
#define SM_MODE3       0x03*0x40  //9 bit variable Uart.
#define BIT_SM2        0x20       //Enable the Multiprocessor communication feature in mode 2&3
                                  //In the mode 2&3 ,if SM2 is set then RI will not be activated
                                  // if the received 9th data bit(RB8)is 0
                                  //In the mode 0 if SM2 is set then the RI will not be acticated
                                  //if a valid stop was not received
#define BIT_REN        0x10       //(R)eceive (En)able
#define BIT_TB8        0x08       // (T)ransmit bit 8
#define BIT_RB8        0x04       //(R)eceive bit 8
#define BIT_TI         0x02       //(T)ransmit (I)nterrup Enable
#define BIT_RI         0x01       //(R)eceive (I)nterrupt flag


/*
 *PCON (P)ower (con)trol
 */
#define BIT_SMOD      0x80
#define BIT_PD        0x02        //(P)ower (D)own  bit
#define BIT_IDL       0x01        //(Idl)e bit


#define SETT1MOD(Mod) (TMOD=((TMOD&T1MOD_CLR)+(Mod<<4)))
#define SETT0MOD(Mod) (TMOD=((TMOD&T0MOD_CLR)+Mod))
#endif // G_REG51_H_INCLUDED
