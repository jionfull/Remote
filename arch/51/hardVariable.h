/*
 * =====================================================================================
 *
 *       Filename:  hardVariable.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2009-12-30 11:30:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */
#include   "..\..\include\Fifo.h"
#include	"config.h"

#ifdef  TX_FIFO_ON
extern unsigned char serialTxBuf[TX_FIFO_MAX];
extern Fifo serialTxFifo;
extern unsigned char txState;
#define TXBUSY  0x01
#define TXENDFOROPT 0x02
#endif     /* -----  not TX_FIFO_ON  ----- */

#ifdef  RX_FIFO_ON
extern unsigned char serialRxBuf[TX_FIFO_MAX];
Fifo serialRxFilo;
#endif     /* -----  not RX_FIFO_ON  ----- */
extern Fifo serialFilo;

