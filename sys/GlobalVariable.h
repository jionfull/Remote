/*
 * =====================================================================================
 *
 *       Filename:  GlobalVariable.h
 *
 *    Description:  全局变量声明
 *
 *        Version:  1.0
 *        Created:  2009-12-29 17:13:50
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  张景福  (jionfull), jionfull@163.com
 *        Company:  
 *
 * =====================================================================================
 */

#ifndef  GLOBALVARIABLE_H_INC
#define  GLOBALVARIABLE_H_INC

#include "..\include\typedef.h"

void GlobalVariable_Init();
extern ticks_t ticks;

#endif   /* ----- #ifndef GLOBALVARIABLE_H_INC  ----- */
